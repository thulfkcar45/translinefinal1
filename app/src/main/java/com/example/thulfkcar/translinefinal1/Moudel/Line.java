package com.example.thulfkcar.translinefinal1.Moudel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by thulfkcar on 11/13/2017.
 */

public class Line {
    //region prop
    int Id;
    String FromCity;
    String Distenation;
    int Type;
    int passCount;
    int passCapacity;
    int passType;
    String GoTime;
    String ReturnTime;
    int TimeType;
    String CarType;
    String DistrictOnLine;
    Boolean CloseComment;
    Boolean CloseRequest;
    Boolean Delete;
    String Title;
    boolean containDriver;


    ArrayList<User> users;


    //endregion

    //region g and s
    public boolean isContainDriver() {
        return containDriver;
    }

    public void setContainDriver(boolean containDriver) {
        this.containDriver = containDriver;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFromCity() {
        return FromCity;
    }

    public void setFromCity(String fromCity) {
        FromCity = fromCity;
    }

    public String getDistenation() {
        return Distenation;
    }

    public void setDistenation(String distenation) {
        Distenation = distenation;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public int getPassCount() {
        return passCount;
    }

    public void setPassCount(int passCount) {
        this.passCount = passCount;
    }

    public int getPassCapacity() {
        return passCapacity;
    }

    public void setPassCapacity(int passCapacity) {
        this.passCapacity = passCapacity;
    }

    public int getPassType() {
        return passType;
    }

    public void setPassType(int passType) {
        this.passType = passType;
    }

    public String getGoTime() {
        return GoTime;
    }

    public void setGoTime(String goTime) {
        GoTime = goTime;
    }

    public String getReturnTime() {
        return ReturnTime;
    }

    public void setReturnTime(String returnTime) {
        ReturnTime = returnTime;
    }

    public int getTimeType() {
        return TimeType;
    }

    public void setTimeType(int timeType) {
        TimeType = timeType;
    }

    public String getCarType() {
        return CarType;
    }

    public void setCarType(String carType) {
        CarType = carType;
    }

    public String getDistrictOnLine() {
        return DistrictOnLine;
    }

    public void setDistrictOnLine(String districtOnLine) {
        DistrictOnLine = districtOnLine;
    }

    public Boolean getCloseComment() {
        return CloseComment;
    }

    public void setCloseComment(Boolean closeComment) {
        CloseComment = closeComment;
    }

    public Boolean getCloseRequest() {
        return CloseRequest;
    }

    public void setCloseRequest(Boolean closeRequest) {
        CloseRequest = closeRequest;
    }

    public Boolean getDelete() {
        return Delete;
    }

    public void setDelete(Boolean delete) {
        Delete = delete;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
    //endregion


    public static void Post_Edit_Add_TransLine(final Context ctx,
                                               Line transLine, String s) {
        final ProgressDialog progress1;

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = s;
            JSONObject jsonBody = new JSONObject();
            if (LocalData.get(ctx, "FLAG_EDIT_LINE").contains("true") ){
                jsonBody.put("Id", LocalData.get(ctx, "CURRENT_LINE_ID"));
                progress1 = ProgressDialog.show(new ContextThemeWrapper(ctx, R.style.progressDialog), "تعديل البيانات",
                        "يرجى الانتظار قليلاً ....", true);
            }
            else
                progress1 = ProgressDialog.show(new ContextThemeWrapper(ctx, R.style.progressDialog), "تسجيل الدخول",
                        "يرجى الانتظار قليلاً ....", true);


            jsonBody.put("UserId", LocalData.get(ctx, "Id"));
            jsonBody.put("Title", transLine.getTitle());
            jsonBody.put("FromCity", transLine.getFromCity());
            jsonBody.put("Distenation", transLine.getDistenation());
            jsonBody.put("DistrictOnLine", transLine.getDistrictOnLine());
            jsonBody.put("Type", transLine.getType());
            jsonBody.put("PassengerType", transLine.getPassType());
            jsonBody.put("PassengersCount", transLine.getPassCount());
            jsonBody.put("PassengersCapacity", transLine.getPassCapacity());
            jsonBody.put("GoTime", transLine.getGoTime());
            jsonBody.put("ReturnTime", transLine.getReturnTime());
            jsonBody.put("TimeType", transLine.getTimeType());
            jsonBody.put("CarType", transLine.getCarType());
            jsonBody.put("CloseComment", transLine.getCloseComment());
            jsonBody.put("CloseRequest", transLine.getCloseRequest());

            final String requestBody = jsonBody.toString();

            StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(ctx, "تم الحفظ بنجاح", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    Log.v("thug", response);
                    Activity activity=(Activity) ctx;
                    CU.onBack(activity);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {

                    String parsed;
                    try {
                        parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    } catch (UnsupportedEncodingException e) {
                        parsed = new String(response.data);
                    }
                    return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            sr.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            requestQueue.add(sr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void PostMake_request(final Context ctx, String UserId, String TranslineId, String RequestState) {
        final ProgressDialog progress1;
        progress1 = ProgressDialog.show(new ContextThemeWrapper(ctx, R.style.progressDialog), "طلب انظمام",
                "يرجى الانتظار قليلاً ....", true);
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = PV.makeRequestUrl();
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("UserId", UserId);
            jsonBody.put("TranslineId", TranslineId);
            jsonBody.put("RequestState", RequestState);
            jsonBody.put("Driver", LocalData.get(ctx ,"FLAG_DRIVER_SATAE"));


            final String requestBody = jsonBody.toString();

            StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(ctx, "تم الحفظ بنجاح", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    Log.v("thug", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {

                    String parsed;
                    try {
                        parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    } catch (UnsupportedEncodingException e) {
                        parsed = new String(response.data);
                    }
                    return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            sr.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            requestQueue.add(sr);
        } catch (JSONException e) {
            progress1.dismiss();
            Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
