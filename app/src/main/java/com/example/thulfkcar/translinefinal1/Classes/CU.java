package com.example.thulfkcar.translinefinal1.Classes;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.thulfkcar.translinefinal1.Fragments.frag_Edit_Profile;
import com.example.thulfkcar.translinefinal1.Fragments.frag_Main;
import com.example.thulfkcar.translinefinal1.Fragments.frag_add_line;
import com.example.thulfkcar.translinefinal1.Fragments.frag_search_line;
import com.example.thulfkcar.translinefinal1.Fragments.frag_sginIn;
import com.example.thulfkcar.translinefinal1.Fragments.frag_singin_method;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.R;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by thulfkcar on 11/13/2017.
 */

public class CU {

    public static void AddAnimation(View view, MotionEvent event) {
        int duration = 150;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(view,
                    "scaleX", 0.9f);
            ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(view,
                    "scaleY", 0.9f);
            scaleDownX.setDuration(duration);
            scaleDownY.setDuration(duration);

            AnimatorSet scaleDown = new AnimatorSet();
            scaleDown.play(scaleDownX).with(scaleDownY);

            scaleDown.start();

            //spinslot();

        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            ObjectAnimator scaleDownX2 = ObjectAnimator.ofFloat(
                    view, "scaleX", 1f);
            ObjectAnimator scaleDownY2 = ObjectAnimator.ofFloat(
                    view, "scaleY", 1f);
            scaleDownX2.setDuration(duration);
            scaleDownY2.setDuration(duration);

            AnimatorSet scaleDown2 = new AnimatorSet();
            scaleDown2.play(scaleDownX2).with(scaleDownY2);

            scaleDown2.start();
        }
    }

    public static void selectSpinnerItemByValue(Spinner spnr, String value) {
        for (int position = 0; position < spnr.getCount(); position++) {
            spnr.setSelection(position);
            String text = spnr.getSelectedItem().toString();
            if (text.equals(value)) {
                break;
            }
        }
    }

    public static void toolbarColor(Activity activity, String color) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor(color));
    }

    public static void toolbarTitle(Activity activity, String title) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
    }

    public static void openfrg(Context ctx, Fragment fragment, Boolean addToBackStack) {
        FragmentActivity activity = (FragmentActivity) ctx;
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);


        //  DetailsFragment newFragment = new DetailsFragment();
        if (addToBackStack)
            fragmentTransaction.replace(R.id.main_conatainer, fragment);
        else
            fragmentTransaction.replace(R.id.main_conatainer, fragment);

        fragmentTransaction.commit();
    }

    public static void nav_menue_items_set(final Activity ctx) {
        NavigationView navigationView = (NavigationView) ctx.findViewById(R.id.nav_view);
        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            ImageView imageView = (ImageView) header.findViewById(R.id.imageView);
            TextView TV_user_inf = (TextView) header.findViewById(R.id.TV_user_inf);
            CardView btn_edit_profile = (CardView) header.findViewById(R.id.btn_edit_profile);
            LinearLayout btn_Logout = (LinearLayout) header.findViewById(R.id.btn_Logout);
            LinearLayout btn_Main = (LinearLayout) header.findViewById(R.id.btn_Main);
            LinearLayout btn_search_line = (LinearLayout) header.findViewById(R.id.btn_search_line);
            LinearLayout btn_create_line = (LinearLayout) header.findViewById(R.id.btn_create_line);

            btn_search_line.setOnTouchListener(new OnSwipeTouchListener(ctx) {
                @Override
                public void onClick() {
                    super.onClick();
                    MainActivity.drawer1.closeDrawers();
                    CU.openfrg(ctx, new frag_search_line(), true);
                }

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    CU.AddAnimation(view, motionEvent);
                    return super.onTouch(view, motionEvent);
                }
            });
            btn_create_line.setOnTouchListener(new OnSwipeTouchListener(ctx) {
                @Override
                public void onClick() {
                    super.onClick();
                    MainActivity.drawer1.closeDrawers();

                    LocalData.add(ctx, "FLAG_EDIT_LINE", "false");


                    CU.openfrg(ctx, new frag_add_line(), true);
                }

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    CU.AddAnimation(view, motionEvent);
                    return super.onTouch(view, motionEvent);
                }
            });

            btn_Main.setOnTouchListener(new OnSwipeTouchListener(ctx) {
                @Override
                public void onClick() {

                    super.onClick();
                    MainActivity.drawer1.closeDrawers();


                    CU.openfrg(ctx, new frag_Main(), false);
                }

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    CU.AddAnimation(view, motionEvent);
                    return super.onTouch(view, motionEvent);
                }
            });

            btn_Logout.setOnTouchListener(new OnSwipeTouchListener(ctx) {
                @Override
                public void onClick() {
                    super.onClick();
                    LoginManager.getInstance().logOut();
                    LocalData.remove(ctx, "Id");
                    FirebaseAuth.getInstance().signOut();
                    MainActivity.drawer1.closeDrawers();


                    CU.openfrg(ctx, new frag_sginIn(), false);
                }

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    CU.AddAnimation(view, motionEvent);
                    return super.onTouch(view, motionEvent);
                }
            });

            btn_edit_profile.setOnTouchListener(new OnSwipeTouchListener(ctx) {
                @Override
                public void onClick() {
                    super.onClick();

                    MainActivity.drawer1.closeDrawers();

                    openfrg(ctx, new frag_Edit_Profile(), true);
                }

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    CU.AddAnimation(view, motionEvent);
                    return super.onTouch(view, motionEvent);
                }
            });
            TV_user_inf.setText(LocalData.get(ctx, "BirthDate")
                    + "\n" + LocalData.get(ctx, "Name")
                    + "\n" + LocalData.get(ctx, "Address")
                    + "\n" + LocalData.get(ctx, "PhoneNum"));

            Glide
                    .with(ctx)
                    .load(LocalData.get(ctx, "Photo"))
                    .placeholder(R.drawable.tl_avatar_icon)
                    .error(R.drawable.tl_avatar_icon)
                    .into(imageView);

            //menu.findItem(R.id.nav_pkg_manage).setVisible(false);//In case you want to remove menu item
            //navigationView.setNavigationItemSelectedListener(ctx);
        }

    }

    private static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private static boolean isInternetAvailable() {
        try {
            final InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
    }

    public static void check_net_connetivity(final Context ctx, final Activity activity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isNetworkAvailable(ctx) && isInternetAvailable()) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx, "Internet available..!!!", Toast.LENGTH_LONG).show();

                        }
                    });
                } else if (!(isNetworkAvailable(ctx)) || !(isInternetAvailable())) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx, "no internet available..!!!", Toast.LENGTH_LONG).show();

                        }
                    });

                }
            }
        }).start();
    }

    public static Boolean onBack(Activity ctx) {

        if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_Main")) {
            return true;
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_add_line")) {
            CU.openfrg(ctx, new frag_Main(), false);

        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_Edit_Profile")) {
            CU.openfrg(ctx, new frag_Main(), false);
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_join_requests")) {
            CU.openfrg(ctx, new frag_Main(), false);
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_chatting")) {
            CU.openfrg(ctx, new frag_Main(), false);
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_phone_auth")) {
            CU.openfrg(ctx, new frag_singin_method(), false);
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_search_line")) {
            CU.openfrg(ctx, new frag_Main(), false);
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_search_result")) {
            CU.openfrg(ctx, new frag_search_line(), false);
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_sginIn")) {
            return true;
        } else if (LocalData.get(ctx, "CURENT_FRAGMENT").equals("frag_singin_method")) {
            CU.openfrg(ctx, new frag_sginIn(), false);
        }

        return false;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String date_befor(Date date) throws ParseException {


        Date current_date=new Date();

        if (date.getYear() < current_date.getYear()) {
            return "قبل" + (current_date.getYear() - date.getYear()) + "سنة";
        } else if (date.getMonth() < current_date.getMonth()) {
            return "قبل" + (current_date.getMonth() - date.getMonth()) + "شهر";
        } else if (date.getDay() < current_date.getDay()) {
            return "قبل" + (current_date.getDay() - date.getDay()) + "يوم";
        }

        final String _24HourTime = date.getHours() + ":" + date.getMinutes();

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
//            System.out.println(_24HourDt);
//            System.out.println(_12HourSDF.format(_24HourDt));

        return _12HourSDF.format(_24HourDt);
    }
}
