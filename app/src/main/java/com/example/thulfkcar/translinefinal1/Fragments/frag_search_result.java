package com.example.thulfkcar.translinefinal1.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.adapters.search_result_adapter;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_search_result extends Fragment {
    View view;
    static Bundle arg;
   GridView frag_search_result_grid;
  SwipeRefreshLayout frag_search_swipe ;
    public frag_search_result() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.set_top_nav(getActivity(), View.GONE);


        //region tool bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView _2_image;
        ImageView _1_image;
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        _2_image.setVisibility(View.VISIBLE);
        _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);
        _1_image.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);

        title.setText("نتائج البحث");


        _2_image.setImageResource(R.drawable.tl_back_icon);
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        // ((DrawerLocker) getActivity()).setDrawerLocked(false);

        CU.toolbarColor(getActivity(), "#FF3D00");
        CU.toolbarTitle(getActivity(), "اضافة خط");
        //endregion

        view =inflater.inflate(R.layout.frag_search_result, container, false);
        frag_search_swipe=(SwipeRefreshLayout)view.findViewById(R.id.frag_search_swipe);
        frag_search_result_grid=(GridView)view.findViewById(R.id.frag_search_result_grid);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalData.add(getContext(),"CURENT_FRAGMENT","frag_search_result");

        ((DrawerLocker) getActivity()).setDrawerLocked(true,false);

         arg = new Bundle(getArguments());

        frag_search_swipe.setRefreshing(true);
PostSearchLine(getContext());


        frag_search_swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PostSearchLine(getContext());

            }
        });

    }

    public void PostSearchLine(final Context ctx) {

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = PV.searchLinesUrl();
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("FromCity", arg.getString("setFromCity"));
            jsonBody.put("Distenation", arg.getString("setDistenation"));
            jsonBody.put("PassengerType", arg.getInt("setPassType"));
            jsonBody.put("TimeType", arg.getInt("setTimeType"));
            jsonBody.put("CarType", arg.getString("setCarType"));
            jsonBody.put("Type", arg.getString("setLineType"));
            jsonBody.put("UserId", LocalData.get(getContext(),"Id"));



            final String requestBody = jsonBody.toString();

            StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(ctx, "النتائج...", Toast.LENGTH_LONG).show();
                    frag_search_swipe.setRefreshing(false);
                    Log.v("thug", response);
                    parseLines(response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
                    frag_search_swipe.setRefreshing(false);
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {

                    String parsed;
                    try {
                        parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    } catch (UnsupportedEncodingException e) {
                        parsed = new String(response.data);
                    }
                    return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            sr.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            requestQueue.add(sr);
        } catch (JSONException e) {
            frag_search_swipe.setRefreshing(false);
            Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

  void  parseLines(String s){
        ArrayList<Line> lines=new ArrayList<>();
        final ArrayList<User> users=new ArrayList<>();

      try {
          JSONArray jsonArray=new JSONArray(s);
          for (int i=0;i<jsonArray.length();i++){
              final JSONObject object=jsonArray.getJSONObject(i);

              users.add(new User(){{
                  setId(object.getInt("UserId"));
                  setName(object.getString("Name"));
                  setPhoneNum(object.getString("PhoneNum"));
              }});
              lines.add(new Line(){{
                  setId(object.getInt("Id"));
                  setFromCity(object.getString("FromCity"));
                  setDistenation(object.getString("Distenation"));
                 if (object.getInt("Type")==1)
                  setContainDriver(true);
                 else
                     setContainDriver(false);

                  setCarType(object.getString("CarType"));
                  setPassCapacity(object.getInt("PassengersCapacity"));
                  setPassCount(object.getInt("PassengersCount"));
                  setDistrictOnLine(object.getString("DistrictOnLine"));
                  setTimeType(object.getInt("TimeType"));
                  setUsers(users);
              }});
          }
          Adapter searchResultAdapter=new search_result_adapter(getActivity(),lines);
          frag_search_result_grid.setAdapter((ListAdapter) searchResultAdapter);
      } catch (JSONException e) {
          e.printStackTrace();
      }






    }
}
