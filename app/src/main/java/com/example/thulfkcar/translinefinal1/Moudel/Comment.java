package com.example.thulfkcar.translinefinal1.Moudel;

import java.util.ArrayList;

/**
 * Created by thulfkcar on 11/14/2017.
 */

public class Comment {
    //region prop
    int Id;
    String Com;
    String Date;
    ArrayList<Line> lines;
    ArrayList<User> users;
    //endregion

    //region g and s
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCom() {
        return Com;
    }

    public void setCom(String com) {
        Com = com;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public ArrayList<Line> getLines() {
        return lines;
    }

    public void setLines(ArrayList<Line> lines) {
        this.lines = lines;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
    //endregion
}
