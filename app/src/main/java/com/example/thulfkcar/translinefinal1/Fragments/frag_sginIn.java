package com.example.thulfkcar.translinefinal1.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.R;


public class frag_sginIn extends Fragment {
    View view;
    LinearLayout btn_driver;
    LinearLayout btn_passenger;

    public frag_sginIn() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //region too bar
        CU.toolbarColor(getActivity(), "#FF9822");
        CU.toolbarTitle(getActivity(), "");


        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        ImageView _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        ImageView _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);

        _2_image.setVisibility(View.GONE);
        _1_image.setVisibility(View.VISIBLE);
        title.setText("");

        //endregion

        MainActivity.set_top_nav(getActivity(), View.GONE);

        view = inflater.inflate(R.layout.frag_sgin_in, container, false);
        MainActivity.set_top_nav(getActivity(), View.GONE);

        btn_driver = (LinearLayout) view.findViewById(R.id.frag_sgin_in_driver);
        btn_passenger = (LinearLayout) view.findViewById(R.id.frag_sgin_in_pass);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((DrawerLocker) getActivity()).setDrawerLocked(false,false);

        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_sginIn");

        btn_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocalData.add(getActivity(), "FLAG_DRIVER_SATAE", "true");


                CU.openfrg(getActivity(), new frag_singin_method(), false);

            }
        });
        btn_passenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocalData.add(getActivity(), "FLAG_DRIVER_SATAE", "false");
                CU.openfrg(getActivity(), new frag_singin_method(), false);

            }
        });


    }


}
