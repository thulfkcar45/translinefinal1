package com.example.thulfkcar.translinefinal1.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.Comment;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;
import com.example.thulfkcar.translinefinal1.adapters.comment_adapter;
import com.example.thulfkcar.translinefinal1.adapters.users_view_adapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_chatting extends Fragment {
    TextView side_txt_line_title;
    LinearLayout side_btn_line_details;
    ImageView side_arrow_details;
    static GridView side_grid_users;
    LinearLayout side_btn_leave;
    CardView side_card_Line_detail;
    LinearLayout side_btn_line_Edit;
    GridView Grid_comments;
    ImageView frag_Line_btn_send_comment;
    EditText frag_Line_txt_comment;
    ScrollView side_scroll_view;
    LinearLayout frag_line_msg_input_layout;

    TextView side_line_details_text_fromcity;
    TextView side_line_details_text_destination;
    TextView side_line_details_text_districtsOnline;
    TextView side_line_details_text_passCapacity;
    TextView side_line_details_text_passCount;
    TextView side_line_details_text_passType;
    TextView side_line_details_text_TimeType;
    TextView side_line_details_text_CarType;
    TextView side_line_details_text_goTime;
    TextView side_line_details_text_returnTime;
    TextView side_line_details_text_comments;
    TextView side_line_details_text_request;
    Bundle arg;
    View view;
    ViewGroup.LayoutParams params;
    private float txt_count;

    public frag_chatting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        {

        }

//region tool bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView _2_image;
        ImageView _1_image;
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        _2_image.setVisibility(View.VISIBLE);
        _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);
        _1_image.setVisibility(View.VISIBLE);
        title.setVisibility(View.GONE);
        if (LocalData.get(getActivity(), "FLAG_DRIVER_SATAE").equals("false")) {

            _2_image.setImageResource(R.drawable.tl_passenger_main_icon);

        } else {

            _2_image.setImageResource(R.drawable.tl_driver_main_icon);


        }
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.drawer2.openDrawer(GravityCompat.END);
            }
        });

        // ((DrawerLocker) getActivity()).setDrawerLocked(false);

        CU.toolbarColor(getActivity(), "#FF3D00");
        CU.toolbarTitle(getActivity(), "اضافة خط");
        //endregion
        view = inflater.inflate(R.layout.frag_line, container, false);
        MainActivity.set_top_nav(getActivity(), view.VISIBLE);
        nav_side_view_set(getActivity());
        //region views
        Grid_comments = (GridView) view.findViewById(R.id.grid_Comments);
        frag_line_msg_input_layout = (LinearLayout) view.findViewById(R.id.frag_line_msg_input_layout);
        frag_Line_btn_send_comment = (ImageView) view.findViewById(R.id.frag_Line_btn_send_comment);
        frag_Line_txt_comment = (EditText) view.findViewById(R.id.frag_Line_txt_comment);
        //endregion

        MainActivity.set_top_nav(getActivity(), View.VISIBLE);

        ((DrawerLocker) getActivity()).setDrawerLocked(true, true);
        frag_Line_txt_comment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                Grid_comments.smoothScrollToPosition(View.FOCUS_DOWN);

            }
        });

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_chatting");
        // MainActivity.frag_line_navigation.setSelectedItemId(R.id.item_main);


        side_grid_users.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        BG_get_users bg_get_users = new BG_get_users(getContext());
        bg_get_users.execute();
        expand_input_msg_txt(Grid_comments, frag_line_msg_input_layout, frag_Line_txt_comment);


        LocalData.add(getContext(), "FLAG_EDIT_LINE", "true");


        LocalData.get(getActivity(), "line_Delete");

        LocalData.get(getActivity(), "line_TimeType");
        LocalData.get(getActivity(), "line_type");

        //region fill controls
        side_line_details_text_fromcity.setText(LocalData.get(getActivity(), "line_from_city"));
        side_line_details_text_destination.setText(LocalData.get(getActivity(), "line_destination"));
        side_line_details_text_districtsOnline.setText(LocalData.get(getActivity(), "line_DistrictOnLine"));

        side_line_details_text_passCapacity.setText(LocalData.get(getActivity(), "line_passCapacity"));

        side_line_details_text_passCount.setText(LocalData.get(getActivity(), "line_passCount"));


        if (Integer.valueOf(LocalData.get(getActivity(), "line_passType")) == 1)
            side_line_details_text_passType.setText("مختلط");
        else if (Integer.valueOf(LocalData.get(getActivity(), "line_passType")) == 2)
            side_line_details_text_passType.setText("ذكور");
        else if (Integer.valueOf(LocalData.get(getActivity(), "line_passType")) == 3)
            side_line_details_text_passType.setText("اناث");
        else if (Integer.valueOf(LocalData.get(getActivity(), "line_passType")) == 4)
            side_line_details_text_passType.setText("حسب الطلب");

        if (Integer.valueOf(LocalData.get(getActivity(), "line_TimeType")) == 1)
            side_line_details_text_TimeType.setText("صباحي");
        else if (Integer.valueOf(LocalData.get(getActivity(), "line_TimeType")) == 2)
            side_line_details_text_TimeType.setText("مسائي");
        else if (Integer.valueOf(LocalData.get(getActivity(), "line_TimeType")) == 3)
            side_line_details_text_TimeType.setText("اي وقت");

        side_line_details_text_CarType.setText(LocalData.get(getActivity(), "line_CarType"));
        side_line_details_text_goTime.setText(LocalData.get(getActivity(), "line_goTime"));
        side_line_details_text_returnTime.setText(LocalData.get(getActivity(), "line_ReturnTime"));
        side_line_details_text_comments.setText(LocalData.get(getActivity(), "line_CloseComment"));
        side_line_details_text_request.setText(LocalData.get(getActivity(), "line_CloseRequest"));
        side_txt_line_title.setText(LocalData.get(getActivity(), "line_Title"));
        //endregion
//getComments

        final ArrayList<Line> lines = new ArrayList<>();

        final ArrayList<User> users = new ArrayList<>();


        final ArrayList<Comment> comments = new ArrayList<>();


        final Adapter adapter = new comment_adapter(getActivity(), comments);
        Grid_comments.setAdapter((ListAdapter) adapter);


// Read from the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(LocalData.get(getActivity(), "CURRENT_LINE_ID"));


        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                final String comment = (String) dataSnapshot.child("comment").getValue();
                final String comment_id = String.valueOf(dataSnapshot.getKey());
                final String date = String.valueOf(dataSnapshot.child("date").getValue());
                final String photo = String.valueOf(dataSnapshot.child("photo").getValue());
                final String userid = String.valueOf(dataSnapshot.child("userid").getValue());
                final String username = String.valueOf(dataSnapshot.child("username").getValue());

                Log.d(TAG, "Value is: " + dataSnapshot);


                lines.add(new Line() {{
                    setId(Integer.parseInt(LocalData.get(getActivity(), "CURRENT_LINE_ID")));
                }});

                users.add(new User() {{
                    setId(Integer.parseInt(userid));
                    setName(username);
                    setPhoto(photo);
                }});

                comments.add(new Comment() {{
                    setCom(comment);
//                        setId(Integer.parseInt(comment_id));
                    setDate(date);
                    setLines(lines);
                    setUsers(users);


                }});
                //  Grid_comments.setAdapter((ListAdapter) adapter);


                ((BaseAdapter) Grid_comments.getAdapter()).notifyDataSetChanged();
                Grid_comments.smoothScrollToPosition(View.FOCUS_DOWN);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        frag_Line_btn_send_comment.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                if (frag_Line_txt_comment.getText().toString().trim().length() > 0) {


                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
                    String currentDateandTime = sdf.format(new Date());
                  //  Log.d("TIME TEST", new Date().toString());

                    Map<String, Object> map = new HashMap<>();
                    map.put("comment", frag_Line_txt_comment.getText().toString());
                    map.put("date", currentDateandTime);
                    map.put("photo", LocalData.get(getActivity(), "Photo"));
                    map.put("userid", LocalData.get(getActivity(), "Id"));
                    map.put("username", LocalData.get(getActivity(), "Name"));
                    final DatabaseReference comment_object_FB = myRef.push();
                    comment_object_FB.setValue(map);
                }
                frag_Line_txt_comment.setText("");

            }


        });


        side_btn_line_Edit.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Fragment fragment = new frag_add_line();

                fragment.setArguments(arg);
                CU.openfrg(getActivity(), fragment, false);
            }
        });

        side_btn_line_details.setOnClickListener(new View.OnClickListener()

        {
            Boolean showLineDetail = true;

            @Override
            public void onClick(View view) {

                if (showLineDetail == true) {
                    showLineDetail = false;
                    side_arrow_details.setRotation(side_arrow_details.getRotation() + 180);
                    side_card_Line_detail.setVisibility(View.VISIBLE);
                } else {
                    showLineDetail = true;
                    side_arrow_details.setRotation(side_arrow_details.getRotation() + 180);
                    side_card_Line_detail.setVisibility(View.GONE);
                }

            }
        });

        LocalData.add(

                getActivity(), "old_value", "1.0");


        side_btn_leave.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                MainActivity.drawer2.closeDrawers();

                Post_Leave_Line(getContext());
            }
        });
    }

    void nav_side_view_set(Activity activity) {
        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view_2);
        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            side_txt_line_title = (TextView) header.findViewById(R.id.side_txt_line_title);
            side_btn_line_details = (LinearLayout) header.findViewById(R.id.side_btn_line_details);
            side_arrow_details = (ImageView) header.findViewById(R.id.side_arrow_details);
            side_grid_users = (GridView) header.findViewById(R.id.side_grid_users);
            side_btn_leave = (LinearLayout) header.findViewById(R.id.side_btn_leave);
            side_card_Line_detail = (CardView) header.findViewById(R.id.side_card_Line_detail);
            side_btn_line_Edit = (LinearLayout) header.findViewById(R.id.side_btn_line_Edit);

//            side_btn_line_details.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    side_btn_line_details.setRotation(180);
//                }
//            });
            side_line_details_text_fromcity = (TextView) header.findViewById(R.id.side_line_details_text_fromcity);
            side_scroll_view = (ScrollView) header.findViewById(R.id.side_scroll_view);
            side_line_details_text_destination = (TextView) header.findViewById(R.id.side_line_details_text_destination);
            side_line_details_text_districtsOnline = (TextView) header.findViewById(R.id.side_line_details_text_districtsOnline);
            side_line_details_text_passCapacity = (TextView) header.findViewById(R.id.side_line_details_text_passCapacity);
            side_line_details_text_passCount = (TextView) header.findViewById(R.id.side_line_details_text_passCount);
            side_line_details_text_passType = (TextView) header.findViewById(R.id.side_line_details_text_passType);
            side_line_details_text_TimeType = (TextView) header.findViewById(R.id.side_line_details_text_TimeType);
            side_line_details_text_CarType = (TextView) header.findViewById(R.id.side_line_details_text_CarType);
            side_line_details_text_goTime = (TextView) header.findViewById(R.id.side_line_details_text_goTime);
            side_line_details_text_returnTime = (TextView) header.findViewById(R.id.side_line_details_text_returnTime);
            side_line_details_text_comments = (TextView) header.findViewById(R.id.side_line_details_text_comments);
            side_line_details_text_request = (TextView) header.findViewById(R.id.side_line_details_text_request);
        }


    }


    void expand_input_msg_txt(final GridView grid_comments, final LinearLayout frag_line_msg_input_layout, final EditText frag_Line_txt_comment) {

        frag_Line_txt_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txt_count = frag_Line_txt_comment.getLineCount();
                params = frag_line_msg_input_layout.getLayoutParams();

                if (txt_count > Float.valueOf(LocalData.get(getActivity(), "old_value"))) {
                    LocalData.add(getActivity(), "old_value", String.valueOf(txt_count));

// Changes the height and width to the specified *pixels*
                    params.height = frag_line_msg_input_layout.getLayoutParams().height + 100;


                } else if (txt_count == Float.valueOf(LocalData.get(getActivity(), "old_value"))) {

                } else {
                    if (frag_Line_txt_comment.getLineCount() == 1) {
                        float o = Float.valueOf(LocalData.get(getActivity(), "old_value"));
                        int g = (int) o;
                        int s = g - 1;
                        params.height = frag_line_msg_input_layout.getLayoutParams().height - 100 * s;
                        LocalData.add(getActivity(), "old_value", String.valueOf(txt_count));

                    } else {
                        params.height = frag_line_msg_input_layout.getLayoutParams().height - 100;
                        LocalData.add(getActivity(), "old_value", String.valueOf(txt_count));

                    }
                }
                frag_line_msg_input_layout.setLayoutParams(params);

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
    }


    public static void Post_Leave_Line(final Context ctx) {
        final ProgressDialog progress1;
        progress1 = ProgressDialog.show(new ContextThemeWrapper(ctx, R.style.progressDialog), "طلب انظمام",
                "يرجى الانتظار قليلاً ....", true);
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = PV.DeleteLineUserUrl();
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("UserId", LocalData.get(ctx, "Id"));
            jsonBody.put("TransId", LocalData.get(ctx, "CURRENT_LINE_ID"));


            final String requestBody = jsonBody.toString();

            StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(ctx, "تم الحفظ بنجاح", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    Log.v("thug", response);
                    CU.openfrg(ctx, new frag_Main(), false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
                    progress1.dismiss();
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {

                    String parsed;
                    try {
                        parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    } catch (UnsupportedEncodingException e) {
                        parsed = new String(response.data);
                    }
                    return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            sr.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            requestQueue.add(sr);
        } catch (JSONException e) {
            progress1.dismiss();
            Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    private class BG_get_users extends AsyncTask<String, Void, String> {
        Context context;
        ArrayList<User> users1 = new ArrayList<User>();

        public BG_get_users(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {

            String UpdateUser_URL = PV.getLineUsersUrl();
            try {
                URL object = new URL(UpdateUser_URL);

                HttpURLConnection con = (HttpURLConnection) object.openConnection();

                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("UserId", LocalData.get(context, "Id"));
                parent.put("TransId", LocalData.get(context, "CURRENT_LINE_ID"));


                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                int HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }


                Log.v("thugEditProfile", result);
                JSONObject root = new JSONObject(result);

                root.getBoolean("Admin");

                JSONObject J_line = root.getJSONObject("Transline");

                final JSONArray J_users = root.getJSONArray("Users");
                for (int i = 0; i < J_line.length(); i++) {
                    final JSONObject m = J_users.getJSONObject(i);
                    users1.add(new User() {{
                        setId(m.getInt("Id"));
                        setFacebookId(m.getString("FacebookId"));
                        setName(m.getString("Name"));
                        setPhoto(m.getString("Photo"));
                        setPhoneNum(m.getString("PhoneNum"));
                        setAddress(m.getString("Address"));
                        setGpsLocation(m.getString("GPSLocation"));
                        setBirthDate(m.getString("BirthDate"));
                        setGender(m.getInt("Gender"));
                        setUserType(m.getInt("UserType"));
                        setAdmin(m.getBoolean("Admin"));
                    }});
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Adapter users_adapter = new users_view_adapter(getActivity(), users1);
            side_grid_users.setAdapter((ListAdapter) users_adapter);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }


}
