package com.example.thulfkcar.translinefinal1.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;
import com.example.thulfkcar.translinefinal1.adapters.join_request_adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_join_requests extends Fragment {
    static ArrayList<User> users = new ArrayList<>();
    static GridView join_grid;
SwipeRefreshLayout frag_join_requests_Swipe;
    View view;

    public frag_join_requests() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LocalData.add(getContext(),"CURENT_FRAGMENT","frag_join_requests");
        ((DrawerLocker) getActivity()).setDrawerLocked(true,true);

        // Inflate the layout for this fragment
        MainActivity.set_top_nav(getActivity(),view.VISIBLE);
        //region tool  bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView _2_image;
        ImageView _1_image;
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        _2_image.setVisibility(View.VISIBLE);
        _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);
        _1_image.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText("طلبات الانظمام");
        _2_image.setImageResource(R.drawable.tl_back_icon);
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        //endregion

        view = inflater.inflate(R.layout.frag_join_requests, container, false);
        join_grid = (GridView) view.findViewById(R.id.join_grid);
        MainActivity.set_top_nav(getActivity(), View.VISIBLE);
        frag_join_requests_Swipe =(SwipeRefreshLayout)view.findViewById(R.id.frag_join_requests_Swipe);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        frag_join_requests_Swipe.setRefreshing(true);
        PostGet_request(getActivity(),
                LocalData.get(getActivity(), "CURRENT_LINE_ID"));

        frag_join_requests_Swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PostGet_request(getActivity(),
                        LocalData.get(getActivity(), "CURRENT_LINE_ID"));
            }
        });

    }


    public void PostGet_request(final Context ctx, String TranslineId) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            String URL = PV.getUsersRequestsUrl();
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("TransId", TranslineId);
            jsonBody.put("UserId", LocalData.get(ctx,"Id"));


            final String requestBody = jsonBody.toString();

            StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    frag_join_requests_Swipe.setRefreshing(false);



                    Log.v("thug", response);
                    users.clear();
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            final JSONObject object = jsonArray.getJSONObject(i);

                            users.add(new User() {{
                                setPhoto(object.getString("Photo"));
                                setId(object.getInt("UserId"));
                                setName(object.getString("Name"));
                                setPhoneNum(object.getString("PhoneNum"));
                                setUserType(object.getInt("UserType"));
                                setAddress(object.getString("Address"));
                                setDate(object.getString("Date"));
                            }});
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Adapter adapter = new join_request_adapter(getActivity(), users);
                    join_grid.setAdapter((ListAdapter) adapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
                    frag_join_requests_Swipe.setRefreshing(false);
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {

                    String parsed;
                    try {
                        parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    } catch (UnsupportedEncodingException e) {
                        parsed = new String(response.data);
                    }
                    return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            sr.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            requestQueue.add(sr);
        } catch (JSONException e) {
            frag_join_requests_Swipe.setRefreshing(false);

            Toast.makeText(ctx, "يوجد خطا في اتصال الخادم", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}
