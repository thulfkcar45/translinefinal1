package com.example.thulfkcar.translinefinal1.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_singin_method extends Fragment {
    LinearLayout frag_signin_method_btn_phone_auth;
    View view;
    CallbackManager callbackManager;
    private LoginButton fb_login_btn;
    private String facebook_id;
    private String f_name;
    private String m_name;
    private String profile_image;
    private String Driver;

    public frag_singin_method() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.set_top_nav(getActivity(), View.GONE);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.frag_singin_method, container, false);
        frag_signin_method_btn_phone_auth = (LinearLayout) view.findViewById(R.id.frag_signin_method_btn_phone_auth);
        fb_login_btn = (LoginButton) view.findViewById(R.id.fb_login_btn);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_singin_method");
        ((DrawerLocker) getActivity()).setDrawerLocked(false,false);

        frag_signin_method_btn_phone_auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CU.openfrg(getActivity(), new frag_phone_auth(), true);
            }
        });


        callbackManager = CallbackManager.Factory.create();
        fb_login_btn.setFragment(this);

        fb_login_btn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Profile profile = Profile.getCurrentProfile();
                if (profile != null) {
                    facebook_id = profile.getId();
                    f_name = profile.getFirstName();
                    m_name = profile.getMiddleName();
                    String l_name = profile.getLastName();
                    String full_name = profile.getName();
                    profile_image = profile.getProfilePictureUri(400, 400).toString();
                    Driver = LocalData.get(getActivity(), "FLAG_DRIVER_SATAE");
                    Log.v("thug", "facebook inf : " + facebook_id + " " + f_name + m_name + " " + l_name + "  " + full_name + "  " + profile_image);
                    BgLoginOrSingup BGWork = new BgLoginOrSingup(getActivity());
                    BGWork.execute(facebook_id, profile_image, f_name + m_name, Driver, "");
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void getFBUserInfo() {
        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            String facebook_id = profile.getId();
            String f_name = profile.getFirstName();
            String m_name = profile.getMiddleName();
            String l_name = profile.getLastName();
            String full_name = profile.getName();
            String profile_image = profile.getProfilePictureUri(400, 400).toString();
            Log.v("thug", facebook_id + " " + f_name + m_name + " " + l_name + "  " + full_name + "  " + profile_image);
        }
    }

    private class BgLoginOrSingup extends AsyncTask<String, Void, String> {
        Context context;
        ProgressDialog progress1;
        private User user;

        //ProgressBar pr;
        BgLoginOrSingup(Context ctx) {
            context = ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            String Facebook = params[0];
            String user_Photo = params[1];
            String UserName = params[2];
            String Driver = params[3];
            String Phone_Num = params[4];
            String login_url = PV.loginOrSignUpUrl();
            try {
                URL object = new URL(login_url);
                HttpURLConnection con = (HttpURLConnection) object.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("Facebook", Facebook);
                parent.put("Photo", user_Photo);
                parent.put("UserName", UserName);
                parent.put("Driver", Driver);
                parent.put("PhoneNum", Phone_Num);


                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                int HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }
                Log.v("thugj", result + "result");
                JSONObject main = new JSONObject(result);
                final int Id = main.getInt("Id");
                final String FacebookId = main.getString("FacebookId");
                final String Name = main.getString("Name");
                final String Photo = main.getString("Photo");
                final String PhoneNum = main.getString("PhoneNum");
                final String GPSLocation = main.getString("GPSLocation");
                final String BirthDate = main.getString("BirthDate");
                final int Gender = main.getInt("Gender");
                final int UserType = main.getInt("UserType");
                final String Address = main.getString("Address");


                user = new User() {{
                    setId(Id);
                    setName(Name);
                    setFacebookId(FacebookId);
                    setPhoto(Photo);
                    setPhoneNum(PhoneNum);
                    setAddress(Address);
                    setGpsLocation(GPSLocation);
                    setBirthDate(BirthDate);

                    setGender(Gender);
                    setUserType(UserType);


                }};
                LocalData.add(context, "Id", String.valueOf(user.getId()));
//                if (UserType == 1)
//                    LocalData.add(context, "FLAG_DRIVER_SATAE", "true");
//                else if (UserType == 2)
//                    LocalData.add(context, "FLAG_DRIVER_SATAE", "false");

                //region shared_prefrences
                LocalData.add(getActivity(), "FaceBookId", user.getFacebookId());
                LocalData.add(getActivity(), "Id", String.valueOf(user.getId()));
                LocalData.add(getActivity(), "Name", user.getName());
                LocalData.add(getActivity(), "Address", user.getAddress());
                LocalData.add(getActivity(), "PhoneNum", user.getPhoneNum());
                LocalData.add(getActivity(), "Photo", user.getPhoto());
                LocalData.add(getActivity(), "Gender", String.valueOf(user.getGender()));
                LocalData.add(getActivity(), "GPSLocation", String.valueOf(user.getGpsLocation()));
                LocalData.add(getActivity(), "BirthDate", String.valueOf(user.getBirthDate()));
                LocalData.add(getActivity(), "Gender", String.valueOf(user.getUserType()));
                //endregion


                Log.v("thugR", Photo);


            } catch (MalformedURLException e) {
                Log.v("thug", e.toString() + "1");
                progress1.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                Log.v("thug", e.toString() + "2");
                progress1.dismiss();
                e.printStackTrace();
            } catch (JSONException e) {
                Log.v("thug", e.toString() + "3");
                progress1.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        protected void onPreExecute() {
            //   fragRegiserDriverBtnRegister.setText("تسجيل الدخول ...");
            progress1 = ProgressDialog.show(new ContextThemeWrapper(getActivity(), R.style.progressDialog), "تسجيل الدخول",
                    "يرجى الانتظار قليلاً ....", true);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                progress1.dismiss();
                CU.openfrg(getActivity(), new frag_Main(), false);

            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }


    }

}
