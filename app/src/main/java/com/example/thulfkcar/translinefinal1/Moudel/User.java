package com.example.thulfkcar.translinefinal1.Moudel;

import java.util.ArrayList;

/**
 * Created by thulfkcar on 11/13/2017.
 */

public class User {
    //region prop
    int id;
    String Photo;
    String FacebookId;
    String Name;
    String PhoneNum;
    String Address;
    String BirthDate;
    String GpsLocation;
    int Gender;
    int UserType;
    float timeRating;
    float drivrRating;
    String Date;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    boolean admin;


    //endregion

    //region g ans s


    public boolean isAdmin() {
        return admin;
    }
    public float getTimeRating() {
        return timeRating;
    }

    public void setTimeRating(float timeRating) {
        this.timeRating = timeRating;
    }

    public float getDrivrRating() {
        return drivrRating;
    }

    public void setDrivrRating(float drivrRating) {
        this.drivrRating = drivrRating;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getFacebookId() {
        return FacebookId;
    }

    public void setFacebookId(String facebookId) {
        FacebookId = facebookId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoneNum() {
        return PhoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        PhoneNum = phoneNum;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getGpsLocation() {
        return GpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        GpsLocation = gpsLocation;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }
    //endregion
}
