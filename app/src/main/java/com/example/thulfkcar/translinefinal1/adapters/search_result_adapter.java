package com.example.thulfkcar.translinefinal1.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.R;

import java.util.ArrayList;

/**
 * Created by thulfkcar on 11/15/2017.
 */

public class search_result_adapter extends BaseAdapter {
    Context ctx;
    LayoutInflater inflater;
    private String phoneNum;

    public search_result_adapter(Context ctx, ArrayList<Line> lines) {
        this.ctx = ctx;
        this.lines = lines;
    }

    ArrayList<Line> lines;

    void fixSize(final View view) {
        if (view != null) {
            view.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            view.getViewTreeObserver().removeOnPreDrawListener(this);
                            view.getLayoutParams().width = (int) (view.getMeasuredWidth() - 100);

//                    params.weight = view.getMeasuredHeight();
                            view.requestLayout();
                            return false;
                        }
                    });
        }
    }

    @Override
    public int getCount() {
        return lines.size();
    }

    @Override
    public Object getItem(int i) {
        return lines.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lines.get(i).getPassCount();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null) {
            view = inflater.inflate(R.layout.row_search_result, viewGroup, false);
        }
        search_result_holder holder = new search_result_holder(view);
        holder.row_search_result_fromCity.setText(lines.get(i).getFromCity());
        holder.row_search_result_destination.setText(lines.get(i).getDistenation());
        if (lines.get(i).isContainDriver())
            holder.row_search_result_isContainDriver.setText("يحتوي سائق");
        else
            holder.row_search_result_isContainDriver.setText("لا يحتوي سائق");
        holder.row_search_result_Car_Type.setText(lines.get(i).getCarType());
        holder.row_search_result_pass_count.setText(String.valueOf(lines.get(i).getPassCount()) + "/" + String.valueOf(lines.get(i).getPassCapacity()));
        holder.row_search_result_District_onLine.setText(lines.get(i).getDistrictOnLine());
        holder.row_search_result_admin_name.setText(lines.get(i).getUsers().get(i).getName());
        phoneNum = lines.get(i).getUsers().get(i).getPhoneNum();
        holder.row_search_result_admin_phoneNum.setText(lines.get(i).getUsers().get(i).getPhoneNum());
        holder.row_search_result_admin_btnDail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(String.valueOf("tel:" + phoneNum)));
                ctx.startActivity(intent);
            }
        });

        holder.row_search_result_btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Line.PostMake_request(
                        ctx,
                        LocalData.get(ctx,"Id"),
                        String.valueOf(lines.get(i).getId()),
                        "Active");
            }
        });
        // fixSize(view);
        return view;
    }
}

class search_result_holder {

    TextView row_search_result_destination;
    TextView row_search_result_isContainDriver;
    TextView row_search_result_fromCity;
    TextView row_search_result_Car_Type;
    TextView row_search_result_pass_count;
    TextView row_search_result_District_onLine;
    TextView row_search_result_admin_name;
    TextView row_search_result_admin_phoneNum;
    LinearLayout row_search_result_btn_join;
    LinearLayout row_search_result_admin_btnDail;

    public search_result_holder(View view) {
        row_search_result_destination = (TextView) view.findViewById(R.id.row_search_result_destination);
        row_search_result_isContainDriver = (TextView) view.findViewById(R.id.row_search_result_isContainDriver);
        row_search_result_fromCity = (TextView) view.findViewById(R.id.row_search_result_fromCity);
        row_search_result_Car_Type = (TextView) view.findViewById(R.id.row_search_result_Car_Type);
        row_search_result_pass_count = (TextView) view.findViewById(R.id.row_search_result_pass_count);
        row_search_result_District_onLine = (TextView) view.findViewById(R.id.row_search_result_District_onLine);
        row_search_result_admin_name = (TextView) view.findViewById(R.id.row_search_result_admin_name);
        row_search_result_admin_phoneNum = (TextView) view.findViewById(R.id.row_search_result_admin_phoneNum);
        row_search_result_btn_join = (LinearLayout) view.findViewById(R.id.row_search_result_btn_join);
        row_search_result_admin_btnDail = (LinearLayout) view.findViewById(R.id.row_search_result_admin_btnDail);
    }
}