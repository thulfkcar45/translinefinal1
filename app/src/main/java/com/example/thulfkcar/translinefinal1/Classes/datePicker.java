package com.example.thulfkcar.translinefinal1.Classes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by thulfkcar on 10/26/2017.
 */

public class datePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    TextView frag_edit_profile_view_Date;
    String dtStart;

    public datePicker(TextView frag_edit_profile_view_Date, String dtStart) {
        this.frag_edit_profile_view_Date = frag_edit_profile_view_Date;
        this.dtStart = dtStart;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = format.parse(dtStart);
            c.setTime(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Use the current date as the default date in the picker
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        month = month + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        frag_edit_profile_view_Date.setText(year + "-" + month + "-" + day);

    }

}
