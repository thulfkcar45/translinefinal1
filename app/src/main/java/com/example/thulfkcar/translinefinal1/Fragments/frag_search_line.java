package com.example.thulfkcar.translinefinal1.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_search_line extends Fragment {
    Spinner frag_search_line_fromCirty;
    EditText frag_search_line_Txt_destination;
    Spinner frag_search_line_Spinner_car_Type;
    Spinner frag_search_line_SpinnerPass_Type;
    Spinner frag_search_line_Spinner_Time_type;
    Button frag_search_line_Btn_Search;
    Spinner frag_search_line_Spinner_LineType;
    View view;


    public frag_search_line() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.set_top_nav(getActivity(), View.GONE);

        // Inflate the layout for this fragment


        //region views
        view = inflater.inflate(R.layout.frag_search_line, container, false);
        frag_search_line_fromCirty = (Spinner) view.findViewById(R.id.frag_search_line_fromCirty);
        frag_search_line_Txt_destination = (EditText) view.findViewById(R.id.frag_search_line_Txt_destination);
        frag_search_line_Spinner_car_Type = (Spinner) view.findViewById(R.id.frag_search_line_Spinner_car_Type);
        frag_search_line_Spinner_LineType = (Spinner) view.findViewById(R.id.frag_search_line_Spinner_LineType);
        frag_search_line_SpinnerPass_Type = (Spinner) view.findViewById(R.id.frag_search_line_SpinnerPass_Type);
        frag_search_line_Spinner_Time_type = (Spinner) view.findViewById(R.id.frag_search_line_Spinner_Time_type);
        frag_search_line_Btn_Search = (Button) view.findViewById(R.id.frag_search_line_Btn_Search);
        //endregion


        //region tool bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView _2_image;
        ImageView _1_image;
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        _2_image.setVisibility(View.VISIBLE);
        _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);
        _1_image.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText("بحث في الخطوط");
        _2_image.setImageResource(R.drawable.tl_back_icon);
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        // ((DrawerLocker) getActivity()).setDrawerLocked(false);

        CU.toolbarColor(getActivity(), "#FF3D00");
        CU.toolbarTitle(getActivity(), "اضافة خط");
        //endregion
        ((DrawerLocker) getActivity()).setDrawerLocked(true,false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalData.add(getContext(),"CURENT_FRAGMENT","frag_search_line");

        frag_search_line_Btn_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Line line = new Line() {{
                    setFromCity(frag_search_line_fromCirty.getSelectedItem().toString());
                    setDistenation(frag_search_line_Txt_destination.getText().toString());
                    int passType = 0;
                    if ((frag_search_line_SpinnerPass_Type.getSelectedItem().toString()).matches("مختلط")) {
                        passType = 1;
                    } else if ((frag_search_line_SpinnerPass_Type.getSelectedItem().toString()).matches("ذكور")) {
                        passType = 2;
                    } else if ((frag_search_line_SpinnerPass_Type.getSelectedItem().toString()).matches("اناث")) {
                        passType = 3;
                    } else if ((frag_search_line_SpinnerPass_Type.getSelectedItem().toString()).matches("حسب الطلب")) {
                        passType = 4;
                    }
                    setPassType(passType);

                    int TimeType = 0;
                    if ((frag_search_line_Spinner_Time_type.getSelectedItem().toString()).matches("صباحي")) {
                        TimeType = 1;
                    } else if ((frag_search_line_Spinner_Time_type.getSelectedItem().toString()).matches("مسائي")) {
                        TimeType = 2;
                    } else if ((frag_search_line_Spinner_Time_type.getSelectedItem().toString()).matches("اي وقت")) {
                        TimeType = 3;
                    }
                    setTimeType(TimeType);

                    if (frag_search_line_Spinner_LineType.getSelectedItem().toString().matches("الكل"))
                        setType(3);
                    else if (frag_search_line_Spinner_LineType.getSelectedItem().toString().matches("يحتوي سائق"))
                        setType(1);
                    else if (frag_search_line_Spinner_LineType.getSelectedItem().toString().matches("لايحتوي سائق"))
                        setType(2);

                        setCarType(frag_search_line_Spinner_car_Type.getSelectedItem().toString());
                }};

                Fragment f = new frag_search_result();
                Bundle arg = new Bundle();
                arg.putString("setFromCity", line.getFromCity());
                arg.putString("setDistenation", line.getDistenation());
                arg.putInt("setPassType", line.getPassType());
                arg.putInt("setTimeType", line.getTimeType());
                arg.putString("setCarType", line.getCarType());
                arg.putString("setLineType", String.valueOf(line.getType()));

                f.setArguments(arg);
                CU.openfrg(getActivity(), f, true);
            }
        });
    }
}
