package com.example.thulfkcar.translinefinal1.Fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_Edit_Profile extends Fragment
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {


    private boolean mRequestingLocationUpdates = false;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    //region Prop
    ProgressDialog progress;

    EditText frag_edit_profile_Txt_Name;
    EditText frag_edit_profile_PhoneNum;
    Spinner frag_edit_profile_Spinner_Gender;
    LinearLayout frag_edit_profile_Btn_GPSLocation;
    Button frag_edit_profile_Btn_Done;
    EditText frag_edit_profile_Txt_Address;
    ImageView frag_edit_profile_Btn_Date_picker;
    String GPSLocation;
    String gender;
    View view;
    LocationManager locationManager;
    LocationListener locationListener;
    int GenderId;
    boolean newUser;

    String mParam1;
    String profile_image;
    String name;
    String facebook_id;
    String Driver;
    private GoogleApiClient mGoogleApiClient;
    private TextView frag_edit_profile_view_Date;
    private SecurityManager LocationController;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    int permissionCheck;
    // LocationRequest locationRequest = LocationRequest.create();

    //endregion

    public frag_Edit_Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_Edit_Profile");
        ((DrawerLocker) getActivity()).setDrawerLocked(false, true);

        permissionCheck = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        //region tool Bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView _2_image;
        ImageView _1_image;
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);
        _2_image.setVisibility(View.VISIBLE);
        _1_image.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText("تعديل بيانات المستخدم");
        _2_image.setImageResource(R.drawable.tl_back_icon);
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        //endregion

//        ((DrawerLocker) getActivity()).setDrawerLocked(false);
        MainActivity.set_top_nav(getActivity(), view.GONE);
        //region views
        view = inflater.inflate(R.layout.frag_edit__profile, container, false);
        frag_edit_profile_Btn_Date_picker = (ImageView) view.findViewById(R.id.frag_edit_profile_Btn_Date_picker);
        frag_edit_profile_Txt_Address = (EditText) view.findViewById(R.id.frag_edit_profile_Txt_Address);
        frag_edit_profile_view_Date = (TextView) view.findViewById(R.id.frag_edit_profile_view_Date);
        frag_edit_profile_Txt_Name = (EditText) view.findViewById(R.id.frag_edit_profile_Txt_Name);
        frag_edit_profile_Spinner_Gender = (Spinner) view.findViewById(R.id.frag_edit_profile_Spinner_Gender);
        frag_edit_profile_Btn_GPSLocation = (LinearLayout) view.findViewById(R.id.frag_edit_profile_Btn_GPSLocation);
        frag_edit_profile_Btn_Done = (Button) view.findViewById(R.id.frag_edit_profile_Btn_Done);
        frag_edit_profile_PhoneNum = (EditText) view.findViewById(R.id.frag_edit_profile_PhoneNum);
        //endregion


        //region fillcontrols
        if (!LocalData.get(getActivity(), "BirthDate").contains("null")) {
            frag_edit_profile_view_Date.setText(LocalData.get(getActivity(), "BirthDate").trim());
        }

        frag_edit_profile_Btn_Date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity ma = (MainActivity) getActivity();
                ma.showDatePickerDialog(frag_edit_profile_view_Date, frag_edit_profile_view_Date.getText().toString());
            }
        });

        if (!LocalData.get(getActivity(), "Address").contains("null")) {

            frag_edit_profile_Txt_Address.setText(LocalData.get(getActivity(), "Address").trim());
        }
        frag_edit_profile_Txt_Name.setText(LocalData.get(getActivity(), "Name"));
        if (!LocalData.get(getActivity(), "PhoneNum").contains("null")) {
            frag_edit_profile_PhoneNum.setText(LocalData.get(getActivity(), "PhoneNum").trim());
        }

        GPSLocation = LocalData.get(getActivity(), "GpsLocation");

        if (LocalData.get(getActivity(), "Gender").contains("1")) {
            CU.selectSpinnerItemByValue(frag_edit_profile_Spinner_Gender, "انثى");
        } else {
            CU.selectSpinnerItemByValue(frag_edit_profile_Spinner_Gender, "ذكر");

        }
        //endregion


        //region dialog
        progress = new ProgressDialog(getContext());
        progress.setProgressStyle(R.style.progressDialog);
        progress.setMessage("يرجى الانتظار قليلاً ....");
        progress.setCancelable(true);
        progress.setIndeterminate(true);
        //endregion
        progress.setTitle("تحديد الموقع");
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }
        frag_edit_profile_Btn_GPSLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progress.show();


                GPSLocation = displayLocation();
                Toast.makeText(getContext(), GPSLocation, Toast.LENGTH_LONG).show();
            }
        });


        frag_edit_profile_Btn_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_controls_text() == true) {


                    //region save_my_data
                    if (frag_edit_profile_Spinner_Gender.getSelectedItem().toString().contains("ذكر")) {
                        GenderId = 2;
                    } else {
                        GenderId = 1;
                    }

                    LocalData.add(getActivity(), "Name", frag_edit_profile_Txt_Name.getText().toString());
                    LocalData.add(getActivity(), "GpsLocation", GPSLocation);
                    LocalData.add(getActivity(), "PhoneNum", frag_edit_profile_PhoneNum.getText().toString());
                    LocalData.add(getActivity(), "Gender", String.valueOf(GenderId));
                    LocalData.add(getActivity(), "Address", frag_edit_profile_Txt_Address.getText().toString());
                    LocalData.add(getActivity(), "BirthDate", frag_edit_profile_view_Date.getText().toString());
                    //endregion


                    //region reques_update
                    bgEditUserProfile bgEditUserProfile = new bgEditUserProfile(getContext());
                    bgEditUserProfile.execute(
                            LocalData.get(getActivity(), "Id"),
                            LocalData.get(getActivity(), "FaceBookId"),
                            frag_edit_profile_Txt_Name.getText().toString(),
                            LocalData.get(getActivity(), "Photo"),
                            frag_edit_profile_PhoneNum.getText().toString(),
                            frag_edit_profile_Txt_Address.getText().toString(),
                            GPSLocation,
                            frag_edit_profile_view_Date.getText().toString(),
                            String.valueOf(GenderId)
                            , "1"

                    );
                    //endregion

                    return;
                }

            }
        });

        MainActivity.set_top_nav(getActivity(), View.GONE);

//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(10000);
//        locationRequest.setFastestInterval(10000 / 2);
//
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
//        builder.setAlwaysShow(true);

//        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//        startActivity(intent);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        // mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }

    private boolean check_controls_text() {
        String sName = frag_edit_profile_Txt_Name.getText().toString();
        if (sName.matches("")) {
            frag_edit_profile_Txt_Name.setHint("يرجى ملئ الاسم");
            frag_edit_profile_Txt_Name.setHintTextColor(Color.parseColor("#ff0000"));
            return false;
        }
        String sPhoneNum = frag_edit_profile_PhoneNum.getText().toString();
        if (sPhoneNum.matches("")) {
            frag_edit_profile_PhoneNum.setHint("يرجى ملئ رقم الهاتف");
            frag_edit_profile_PhoneNum.setHintTextColor(Color.parseColor("#ff0000"));
            return false;
        }
        String sAddress = frag_edit_profile_Txt_Address.getText().toString();
        if (sAddress.matches("")) {
            frag_edit_profile_Txt_Address.setHint("يرجى ملئ العنوان");
            frag_edit_profile_Txt_Address.setHintTextColor(Color.parseColor("#ff0000"));
            return false;
        }
        String sGPSLocation = GPSLocation;
        if (sGPSLocation.matches("")) {
            Toast.makeText(getActivity(), "يرجى تحديد الموقع", Toast.LENGTH_LONG).show();
            return false;
        }
        String sView_date = frag_edit_profile_view_Date.getText().toString();
        if (sView_date.matches("")) {
            frag_edit_profile_view_Date.setHint("يرجى ملئ تأريخ الميلاد");
            frag_edit_profile_view_Date.setHintTextColor(Color.parseColor("#ff0000"));
            return false;
        }

        return true;
    }

    /**
     * Method to display the location on UI
     */
    private String displayLocation() {
        double latitude = 0;
        double longitude = 0;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
// TODO: Consider calling


            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
        Location mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

            //  Toast.makeText(getContext(), latitude + "," + longitude, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getContext(), "(Couldn't get the location. Make sure location is enabled on the device)", Toast.LENGTH_LONG).show();
        }
        progress.dismiss();
        return latitude + "," + longitude;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        statusCheck();
        checkPlayServices();

    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());

    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        displayLocation();

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    private class bgEditUserProfile extends AsyncTask<String, Void, String> {
        Context context;
        ProgressDialog progress1;

        public bgEditUserProfile(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {

            String Id = params[0];
            String FacebookId = params[1];
            String Name = params[2];
            String Photo = params[3];
            String PhoneNum = params[4];
            String Address = params[5];
            String GPSLocation = params[6];
            String BirthDate = params[7];
            String Gender = params[8];
            String UserType = params[9];


            String UpdateUser_URL = PV.updateUserUrl();
            try {
                URL object = new URL(UpdateUser_URL);

                HttpURLConnection con = (HttpURLConnection) object.openConnection();

                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("Id", Id);
                parent.put("FacebookId", FacebookId);
                parent.put("Name", Name);
                parent.put("Photo", Photo);
                parent.put("PhoneNum", PhoneNum);
                parent.put("Address", Address);
                parent.put("GPSLocation", GPSLocation);
                parent.put("BirthDate", BirthDate);
                parent.put("Gender", Gender);
                parent.put("UserType", UserType);


                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                int HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }


                Log.v("thugEditProfile", result);

                CU.openfrg(getActivity(), new frag_Main(), false);


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            CU.nav_menue_items_set(getActivity());
            progress1.dismiss();
//            Fragment fragment =fragPasengerMain.newInstance(true);
//            CU.openfrg(context,fragment, false);
            CU.onBack(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress1 = ProgressDialog.show(new ContextThemeWrapper(getActivity(), R.style.progressDialog), "تعديل البيانات",
                    "يرجى الانتظار قليلاً ....", true);
        }
    }


    public void statusCheck() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("يرجى تشغيل تحديد الموقع")
                .setCancelable(false)
                .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
