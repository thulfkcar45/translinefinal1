package com.example.thulfkcar.translinefinal1.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Moudel.Comment;
import com.example.thulfkcar.translinefinal1.R;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by thulfkcar on 11/14/2017.
 */

public class comment_adapter extends BaseAdapter {
    LayoutInflater inflater;
    Context ctx;
    ArrayList<Comment> comments;

    public comment_adapter(Context ctx, ArrayList<Comment> comments) {
        this.ctx = ctx;
        this.comments = comments;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int i) {
        return comments.get(i);
    }

    @Override
    public long getItemId(int i) {
        return comments.get(i).getId();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null) {
            view = inflater.inflate(R.layout.row_comment, viewGroup, false);
        }
        comment_holder comment_holder = new comment_holder(view);
        Glide
                .with(ctx)
                .load(comments.get(i).getUsers().get(i).getPhoto())
                .placeholder(R.drawable.tl_avatar_icon)
                .error(R.drawable.tl_avatar_icon)
                .into(comment_holder.row_com_userImage);

        comment_holder.row_com_comment.setText(comments.get(i).getCom());

        comment_holder.row_com_username.setText(comments.get(i).getUsers().get(i).getName());





        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
        try {
            Date  convertedDate = sdf.parse(comments.get(i).getDate());
            comment_holder.row_com_date.setText(CU.date_befor(convertedDate));

        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (comments.get(i).getUsers().get(i).getId() == Integer.valueOf(LocalData.get(ctx, "Id"))) {
            view.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            comment_holder.row_com_background.setCardBackgroundColor(Color.parseColor("#FF3D00"));
            comment_holder.row_com_username.setVisibility(View.GONE);
            comment_holder.row_com_date.setTextColor(Color.parseColor("#ffd66b"));
            comment_holder.row_com_comment.setTextColor(Color.parseColor("#FFFFFF"));


        } else {
            view.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            comment_holder.row_com_background.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
            comment_holder.row_com_username.setVisibility(View.VISIBLE);
            comment_holder.row_com_username.setTextColor(Color.parseColor("#000000"));
            comment_holder.row_com_date.setTextColor(Color.parseColor("#9e9e9e"));
            comment_holder.row_com_comment.setTextColor(Color.parseColor("#FF3D00"));
        }
        return view;
    }
}

class comment_holder {
    ImageView row_com_userImage;
    TextView row_com_username;
    TextView row_com_date;
    CardView row_com_background;
    TextView row_com_comment;

    public comment_holder(View view) {
        row_com_userImage = (ImageView) view.findViewById(R.id.row_com_userImage);
        row_com_username = (TextView) view.findViewById(R.id.row_com_username);
        row_com_comment = (TextView) view.findViewById(R.id.row_com_comment);
        row_com_date = (TextView) view.findViewById(R.id.row_com_date);
        row_com_background = (CardView) view.findViewById(R.id.row_com_background);
    }
}