package com.example.thulfkcar.translinefinal1.Fragments;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.Classes.SetTime;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */

public class frag_add_line extends Fragment {

    View view;
    EditText frag_add_line_txt_titel;
    TextView frag_add_line_txt_Go_time;
    EditText frag_add_line_txt_District_onLine;
    EditText frag_add_line_txt_Pass_Count;
    EditText frag_add_line_txt_pass_Capacity;
    Spinner frag_add_line_txt_Spinner_FromCity;
    EditText frag_add_line_txt_Destination;
    Spinner frag_add_line_Spinner_Pass_Type;
    Spinner frag_add_line_Spinner_Time_Type;
    Spinner frag_add_line_Spinner_car_Type;
    LinearLayout frag_add_line_Dialog_Go_time;
    LinearLayout frag_add_line_Dialog_return_time;
    Switch frag_add_line_Switch_Comment;
    Switch frag_add_line_Switch_Request;
    Button frag_add_line_btn_save;
    private TextView frag_add_line_txt_return_time;

    private void clear() {
        frag_add_line_txt_titel.setText("");
        frag_add_line_txt_District_onLine.setText("");
        frag_add_line_txt_Pass_Count.setText("");
        frag_add_line_txt_pass_Capacity.setText("");
        frag_add_line_txt_Destination.setText("");
        frag_add_line_txt_Go_time.setText("");
        frag_add_line_txt_return_time.setText("");
        frag_add_line_txt_titel.setText("");
    }

    public frag_add_line() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //region tool bar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView _2_image;
        ImageView _1_image;
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        _2_image.setVisibility(View.VISIBLE);
        _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);
        _1_image.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText("اضافة خط");
        _2_image.setImageResource(R.drawable.tl_back_icon);
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        // ((DrawerLocker) getActivity()).setDrawerLocked(false);

        CU.toolbarColor(getActivity(), "#FF3D00");
        CU.toolbarTitle(getActivity(), "اضافة خط");
        //endregion
        // Inflate the layout for this fragment
        //region view
        view = inflater.inflate(R.layout.frag_add_line, container, false);
        frag_add_line_txt_titel = (EditText) view.findViewById(R.id.frag_add_line_txt_titel);
        frag_add_line_txt_Pass_Count = (EditText) view.findViewById(R.id.frag_add_line_txt_Pass_Count);
        frag_add_line_txt_Go_time = (TextView) view.findViewById(R.id.frag_add_line_txt_Go_time);
        frag_add_line_txt_return_time = (TextView) view.findViewById(R.id.frag_add_line_txt_return);
        frag_add_line_txt_District_onLine = (EditText) view.findViewById(R.id.frag_add_line_txt_District_onLine);
        frag_add_line_txt_pass_Capacity = (EditText) view.findViewById(R.id.frag_add_line_txt_pass_Capacity);
        frag_add_line_txt_Spinner_FromCity = (Spinner) view.findViewById(R.id.frag_add_line_txt_Spinner_FromCity);
        frag_add_line_txt_Destination = (EditText) view.findViewById(R.id.frag_add_line_txt_Destination);
        frag_add_line_Spinner_Pass_Type = (Spinner) view.findViewById(R.id.frag_add_line_Spinner_Pass_Type);
        frag_add_line_Spinner_Time_Type = (Spinner) view.findViewById(R.id.frag_add_line_Spinner_Time_Type);
        frag_add_line_Spinner_car_Type = (Spinner) view.findViewById(R.id.frag_add_line_Spinner_car_Type);
        frag_add_line_Dialog_Go_time = (LinearLayout) view.findViewById(R.id.frag_add_line_Dialog_Go_time);
        frag_add_line_Dialog_return_time = (LinearLayout) view.findViewById(R.id.frag_add_line_Dialog_return_time);
        frag_add_line_Switch_Comment = (Switch) view.findViewById(R.id.frag_add_line_Switch_Comment);
        frag_add_line_Switch_Request = (Switch) view.findViewById(R.id.frag_add_line_Switch_requests);
        frag_add_line_btn_save = (Button) view.findViewById(R.id.frag_add_line_btn_save);
        //endregion
        MainActivity.set_top_nav(getActivity(), View.GONE);
        ((DrawerLocker) getActivity()).setDrawerLocked(true,false);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_add_line");
        MainActivity.set_top_nav(getActivity(), view.GONE);


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CU.hideKeyboardFrom(getContext(), view);
                return false;
            }
        });

        String flag_line = LocalData.get(getContext(), "FLAG_EDIT_LINE");

        if (flag_line.contains("false")) {
            add_line();
        } else {
            edit_line();
        }
    }

    void edit_line() {
        fill_contols_to_edit();

        frag_add_line_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //add Line
                User user = new User() {{
//            setId(Integer.parseInt(LocalData.get(ctx,"Id")));
                    setId(Integer.parseInt(LocalData.get(getContext(), "Id")));
                }};
                final ArrayList<User> users = new ArrayList<User>();
                users.add(user);


                if (!controls_text_isEmpty()) {
                    Line line = new Line() {{
                        setUsers(users);
                        if (LocalData.get(getActivity(), "FLAG_DRIVER_SATAE").contains("true")) {
                            setType(1);
                        } else
                            setType(2);
                        setTitle(frag_add_line_txt_titel.getText().toString());
                        setPassCount(Integer.parseInt(frag_add_line_txt_Pass_Count.getText().toString()));
                        setGoTime(frag_add_line_txt_Go_time.getText().toString());
                        setReturnTime(frag_add_line_txt_return_time.getText().toString());
                        setDistrictOnLine(frag_add_line_txt_District_onLine.getText().toString());
                        setPassCapacity(Integer.parseInt(frag_add_line_txt_pass_Capacity.getText().toString()));
                        setFromCity(frag_add_line_txt_Spinner_FromCity.getSelectedItem().toString());
                        setDistenation(frag_add_line_txt_Destination.getText().toString());
                        int passType = 0;
                        if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("مختلط")) {
                            passType = 1;
                        } else if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("ذكور")) {
                            passType = 2;
                        } else if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("اناث")) {
                            passType = 3;
                        } else if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("حسب الطلب")) {
                            passType = 4;
                        }
                        setPassType(passType);

                        int TimeType = 0;
                        if ((frag_add_line_Spinner_Time_Type.getSelectedItem().toString()).matches("صباحي")) {
                            TimeType = 1;
                        } else if ((frag_add_line_Spinner_Time_Type.getSelectedItem().toString()).matches("مسائي")) {
                            TimeType = 2;
                        } else if ((frag_add_line_Spinner_Time_Type.getSelectedItem().toString()).matches("اي وقت")) {
                            TimeType = 3;
                        }
                        setTimeType(TimeType);
                        setCarType(frag_add_line_Spinner_car_Type.getSelectedItem().toString());
                        setGoTime(frag_add_line_txt_Go_time.getText().toString());
                        setReturnTime(frag_add_line_txt_return_time.getText().toString());
                        setCloseComment(frag_add_line_Switch_Comment.isChecked());
                        setCloseRequest(frag_add_line_Switch_Request.isChecked());
                    }};


                    Line.Post_Edit_Add_TransLine(getActivity(),
                            line,
                            PV.updateLineUrl()
                    );
                }
            }
        });
    }

    void add_line() {
        new SetTime(frag_add_line_txt_Go_time, frag_add_line_Dialog_Go_time, getActivity());
        new SetTime(frag_add_line_txt_return_time, frag_add_line_Dialog_return_time, getActivity());
        frag_add_line_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //add Line
                User user = new User() {{
//            setId(Integer.parseInt(LocalData.get(ctx,"Id")));
                    setId(Integer.parseInt(LocalData.get(getContext(), "Id")));
                }};
                final ArrayList<User> users = new ArrayList<User>();
                users.add(user);


                if (!controls_text_isEmpty()) {
                    Line line = new Line() {{
                        setUsers(users);
                        setTitle(frag_add_line_txt_titel.getText().toString());
                        setPassCount(Integer.parseInt(frag_add_line_txt_Pass_Count.getText().toString()));
                        setGoTime(frag_add_line_txt_Go_time.getText().toString());
                        setReturnTime(frag_add_line_txt_return_time.getText().toString());
                        setDistrictOnLine(frag_add_line_txt_District_onLine.getText().toString());
                        setPassCapacity(Integer.parseInt(frag_add_line_txt_pass_Capacity.getText().toString()));
                        setFromCity(frag_add_line_txt_Spinner_FromCity.getSelectedItem().toString());
                        setDistenation(frag_add_line_txt_Destination.getText().toString());
                        int passType = 0;
                        if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("مختلط")) {
                            passType = 1;
                        } else if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("ذكور")) {
                            passType = 2;
                        } else if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("اناث")) {
                            passType = 3;
                        } else if ((frag_add_line_Spinner_Pass_Type.getSelectedItem().toString()).matches("حسب الطلب")) {
                            passType = 4;
                        }
                        setPassType(passType);

                        int TimeType = 0;
                        if ((frag_add_line_Spinner_Time_Type.getSelectedItem().toString()).matches("صباحي")) {
                            TimeType = 1;
                        } else if ((frag_add_line_Spinner_Time_Type.getSelectedItem().toString()).matches("مسائي")) {
                            TimeType = 2;
                        } else if ((frag_add_line_Spinner_Time_Type.getSelectedItem().toString()).matches("اي وقت")) {
                            TimeType = 3;
                        }
                        setTimeType(TimeType);
                        setCarType(frag_add_line_Spinner_car_Type.getSelectedItem().toString());
                        setGoTime(frag_add_line_txt_Go_time.getText().toString());
                        setReturnTime(frag_add_line_txt_return_time.getText().toString());
                        setCloseComment(frag_add_line_Switch_Comment.isChecked());
                        setCloseRequest(frag_add_line_Switch_Request.isChecked());
                        if (LocalData.get(getActivity(), "FLAG_DRIVER_SATAE").contains("true")) {
                            setType(1);
                        } else
                            setType(2);
                    }};


                    Line.Post_Edit_Add_TransLine(getActivity(),
                            line,
                            PV.getAddLineUrl()
                    );
                }
            }
        });


    }

    private Boolean controls_text_isEmpty() {
        String title = frag_add_line_txt_titel.getText().toString();
        if (title.matches("")) {
            frag_add_line_txt_titel.setHint("يرجى ملئ العنوان");
            frag_add_line_txt_titel.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }
        String District_onLine = frag_add_line_txt_District_onLine.getText().toString();
        if (District_onLine.matches("")) {
            frag_add_line_txt_District_onLine.setHint("يرجى ملئ المناطق المار بها");
            frag_add_line_txt_District_onLine.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }
        String Pass_Count = frag_add_line_txt_Pass_Count.getText().toString();
        if (Pass_Count.matches("")) {
            frag_add_line_txt_Pass_Count.setHint("يرجى ملئ المناطق المقاعد المشغولة");
            frag_add_line_txt_Pass_Count.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }
        String pass_Capacity = frag_add_line_txt_pass_Capacity.getText().toString();
        if (pass_Capacity.matches("")) {
            frag_add_line_txt_pass_Capacity.setHint("يرجى ملئ المقاعد الكلية");
            frag_add_line_txt_pass_Capacity.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }

        String Destination = frag_add_line_txt_Destination.getText().toString();
        if (Destination.matches("")) {
            frag_add_line_txt_Destination.setHint("يرجى ملئ الوجهة");
            frag_add_line_txt_Destination.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }

        String txt_Go_time = frag_add_line_txt_Go_time.getText().toString();
        if (txt_Go_time.matches("")) {
            frag_add_line_txt_Go_time.setHint("يرجى اختيار وقت الذهاب");
            frag_add_line_txt_Go_time.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }
        String txt_return = frag_add_line_txt_return_time.getText().toString();
        if (txt_return.matches("")) {
            frag_add_line_txt_return_time.setHint("يرجى اختيار وقت العودة");
            frag_add_line_txt_return_time.setHintTextColor(Color.parseColor("#ff0000"));
            return true;
        }

        return false;
    }

    void fill_contols_to_edit() {

//        Bundle arg = new Bundle(getArguments());

        // arg.getInt("line_type");
        // arg.getBoolean("line_Delete");


        //  frag_add_line_txt_titel.setText(arg.getInt("line_id"));

        LocalData.get(getActivity(),"line_type");
        LocalData.get(getActivity(),"line_Delete");

        LocalData.get(getActivity(), "CURRENT_LINE_ID");

        frag_add_line_txt_Pass_Count.setText(  LocalData.get(getActivity(),"line_passCount"));
        frag_add_line_txt_pass_Capacity.setText(LocalData.get(getActivity(),"line_passCapacity"));

        if ( Integer.valueOf(LocalData.get(getActivity(),"line_passType")) == 1)
            selectSpinnerItemByValue(frag_add_line_Spinner_Pass_Type, "مختلط");
        else if (Integer.valueOf(LocalData.get(getActivity(),"line_passType"))== 2)
            selectSpinnerItemByValue(frag_add_line_Spinner_Pass_Type, "ذكور");
        else if (Integer.valueOf(LocalData.get(getActivity(),"line_passType")) == 3)
            selectSpinnerItemByValue(frag_add_line_Spinner_Pass_Type, "اناث");
        else if (Integer.valueOf(LocalData.get(getActivity(),"line_passType")) == 4)
            selectSpinnerItemByValue(frag_add_line_Spinner_Pass_Type, "حسب الطلب");


        if (Integer.valueOf(LocalData.get(getActivity(),"line_TimeType"))== 1)
            selectSpinnerItemByValue(frag_add_line_Spinner_Time_Type, "صباحي");
        else if (Integer.valueOf(LocalData.get(getActivity(),"line_TimeType")) == 2)
            selectSpinnerItemByValue(frag_add_line_Spinner_Time_Type, "مسائي");
        else if (Integer.valueOf(LocalData.get(getActivity(),"line_TimeType")) == 3)
            selectSpinnerItemByValue(frag_add_line_Spinner_Time_Type, "اي وقت");


        selectSpinnerItemByValue(frag_add_line_txt_Spinner_FromCity,LocalData.get(getActivity(),"line_from_city"));
        selectSpinnerItemByValue(frag_add_line_Spinner_car_Type, LocalData.get(getActivity(),"line_CarType"));


        frag_add_line_txt_Destination.setText(LocalData.get(getActivity(),"line_destination"));
        frag_add_line_txt_Go_time.setText( LocalData.get(getActivity(),"line_goTime"));
        frag_add_line_txt_return_time.setText( LocalData.get(getActivity(),"line_ReturnTime"));
        frag_add_line_txt_District_onLine.setText( LocalData.get(getActivity(),"line_DistrictOnLine"));
        frag_add_line_txt_titel.setText( LocalData.get(getActivity(),"line_Title"));

        frag_add_line_Switch_Comment.setChecked( Boolean.valueOf(LocalData.get(getActivity(),"line_CloseComment")));
        frag_add_line_Switch_Request.setChecked(Boolean.valueOf( LocalData.get(getActivity(),"line_CloseRequest")));

    }

    public static void selectSpinnerItemByValue(Spinner spnr, String value) {
        for (int position = 0; position < spnr.getCount(); position++) {
            spnr.setSelection(position);
            String text = spnr.getSelectedItem().toString();
            if (text.equals(value)) {
                break;
            }
        }
    }


}
