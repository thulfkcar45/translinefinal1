package com.example.thulfkcar.translinefinal1;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.datePicker;
import com.example.thulfkcar.translinefinal1.Fragments.frag_Main;
import com.example.thulfkcar.translinefinal1.Fragments.frag_chatting;
import com.example.thulfkcar.translinefinal1.Fragments.frag_join_requests;
import com.example.thulfkcar.translinefinal1.Fragments.frag_sginIn;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DrawerLocker {
    public static BottomNavigationView frag_line_navigation;
    public static DrawerLayout drawer1;
    private ActionBarDrawerToggle toggle1;
    public static DrawerLayout drawer2;
    private ActionBarDrawerToggle toggle2;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/CairoRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //  getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        notification();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(Drawable.createFromPath("drawable/btn_whigt_shape.xml"));
        //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        drawer1 = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle1 = new ActionBarDrawerToggle(
                this, drawer1, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer1.addDrawerListener(toggle1);
        toggle1.syncState();

        drawer2 = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle2 = new ActionBarDrawerToggle(
                this, drawer2, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer2.addDrawerListener(toggle2);
        toggle2.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        checkLoginState(savedInstanceState);

        frag_line_navigation = (BottomNavigationView) findViewById(R.id.frag_line_navigation);

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Activity activity = (Activity) this;
            if (CU.onBack(activity)) {
                super.onBackPressed();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void showDatePickerDialog(TextView textView, String date) {
        DialogFragment newFragment = new datePicker(textView, date);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void set_top_nav(final Activity activity, int visiblity) {

        frag_line_navigation.setVisibility(visiblity);
        frag_line_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.item_main)
                    CU.openfrg(activity, new frag_chatting(), true);
                else if (item.getItemId() == R.id.item_requests) {

                    CU.openfrg(activity, new frag_join_requests(), true);

                }
                return true;
            }
        });
    }

    private void checkLoginState(Bundle savedInstanceState) {

        String UDID = LocalData.get(this.getApplication(), "Id");
        if (UDID.length() > 0) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.main_conatainer, new frag_Main())
                    .commit();

        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.main_conatainer, new frag_sginIn())
                    .commit();

        }
        if (findViewById(R.id.main_conatainer) != null) {

            if (savedInstanceState != null) {

                return;
            }


        }


    }


    @Override
    public void setDrawerLocked(Boolean d1, Boolean d2) {
        if (d1 == true) {
            drawer1.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.START);
            toggle1.setDrawerIndicatorEnabled(true);

            Log.v("thugDrawer", "    true");
        } else {
            drawer1.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.START);
            toggle1.setDrawerIndicatorEnabled(false);


            Log.v("thugDrawer", "    false");

        }

        if (d2 == true) {
            drawer2.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.END);

        } else {


            drawer2.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);

        }
    }
    void notification() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        // If a notification message is tapped, any data accompanying the notification
        // message is available in the intent extras. In this sample the launcher
        // intent is fired when the notification is tapped, so any accompanying data would
        // be handled here. If you want a different intent fired, set the click_action
        // field of the notification message to the desired intent. The launcher intent
        // is used when no click_action is specified.
        //
        // Handle possible data accompanying notification message.
        // [START handle_data_extras]
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d("thug", "Key: " + key + " Value: " + value);
            }
        }
        // [END handle_data_extras]


        // [START subscribe_topics]
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        // [END subscribe_topics]

        // Log and toast
        String msg = getString(R.string.msg_subscribed);

        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();


        // Get token
        String token = FirebaseInstanceId.getInstance().getToken();

        // Log and toast
        String mssg = getString(R.string.msg_token_fmt, token);
        Toast.makeText(MainActivity.this, mssg, Toast.LENGTH_SHORT).show();
        Log.v("token", mssg);


    }


}
