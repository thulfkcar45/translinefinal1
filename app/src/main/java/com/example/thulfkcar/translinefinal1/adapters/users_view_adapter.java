package com.example.thulfkcar.translinefinal1.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;

import java.util.ArrayList;

/**
 * Created by thulfkcar on 11/14/2017.
 */

public class users_view_adapter extends BaseAdapter {
    Context ctx;
    LayoutInflater inflater;

    public users_view_adapter(Context ctx, ArrayList<User> users) {
        this.ctx = ctx;
        this.users = users;
    }

    ArrayList<User> users;

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return users.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null) {
            view = inflater.inflate(R.layout.row_user, viewGroup, false);
        }
        users_view_holder holder = new users_view_holder(view);
        holder.row_user_name.setText(users.get(i).getName());

        Glide
                .with(ctx)
                .load(users.get(i).getPhoto())
                .placeholder(R.drawable.tl_avatar_icon)
                .error(R.drawable.tl_avatar_icon)
                .into(holder.row_user_photo);


        if (users.get(i).isAdmin()) {
            holder.row_user_state.setTextColor(Color.parseColor("#00E676"));
            holder.row_user_state.setText("مسؤول");
        } else {
            holder.row_user_state.setTextColor(Color.parseColor("#9C9C9C"));

            holder.row_user_state.setText("عضو");
        }
        if (users.get(i).getUserType() == 1) {
            holder.row_user_type.setTextColor(Color.parseColor("#FF8C14"));

            holder.row_user_type.setText("سائق");
        } else {
            holder.row_user_type.setTextColor(Color.parseColor("#FFFFFF"));

            holder.row_user_type.setText("راكب");
        }

        return view;
    }
}

class users_view_holder {
    TextView row_user_type;
    TextView row_user_name;
    TextView row_user_state;
    ImageView row_user_photo;

    public users_view_holder(View view) {
        row_user_type = (TextView) view.findViewById(R.id.row_user_type);
        row_user_name = (TextView) view.findViewById(R.id.row_user_name);
        row_user_state = (TextView) view.findViewById(R.id.row_user_state);
        row_user_photo = (ImageView) view.findViewById(R.id.row_user_photo);
    }
}