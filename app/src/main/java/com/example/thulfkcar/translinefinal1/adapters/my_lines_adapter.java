package com.example.thulfkcar.translinefinal1.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.OnSwipeTouchListener;
import com.example.thulfkcar.translinefinal1.Fragments.frag_chatting;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.R;

import java.util.ArrayList;

/**
 * Created by thulfkcar on 11/13/2017.
 */

public class my_lines_adapter extends BaseAdapter {
    Context ctx;
    LayoutInflater inflater;
    private Bundle arg = new Bundle();

    public my_lines_adapter(Context ctx, ArrayList<Line> lines) {
        this.ctx = ctx;
        this.lines = lines;
    }

    ArrayList<Line> lines;

    @Override
    public int getCount() {
        return lines.size();
    }

    @Override
    public Object getItem(int i) {
        return lines.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lines.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null) {
            view = inflater.inflate(R.layout.row_line_header, viewGroup, false);
        }
        my_lines_holder my_lines_holder = new my_lines_holder(view);


        my_lines_holder.capacity.setText(String.valueOf(lines.get(i).getPassCount()) + "/" + String.valueOf(lines.get(i).getPassCapacity()));
        my_lines_holder.title.setText(lines.get(i).getTitle());
        my_lines_holder.from_city.setText(lines.get(i).getFromCity());
        my_lines_holder.destination.setText(lines.get(i).getDistenation());

        view.setOnTouchListener(new OnSwipeTouchListener(ctx) {
            @Override
            public void onClick() {
                super.onClick();

                LocalData.add(ctx,"line_id", String.valueOf(lines.get(i).getId()));
                LocalData.add(ctx,"line_from_city", lines.get(i).getFromCity());
                LocalData.add(ctx,"line_destination", lines.get(i).getDistenation());
                LocalData.add(ctx,"line_type", String.valueOf(lines.get(i).getType()));
                LocalData.add(ctx,"line_passCount", String.valueOf(lines.get(i).getPassCount()));
                LocalData.add(ctx,"line_passCapacity", String.valueOf(lines.get(i).getPassCapacity()));
                LocalData.add(ctx,"line_passType", String.valueOf(lines.get(i).getPassType()));
                LocalData.add(ctx,"line_goTime", lines.get(i).getGoTime());
                LocalData.add(ctx,"line_ReturnTime", lines.get(i).getReturnTime());
                LocalData.add(ctx,"line_TimeType", String.valueOf(lines.get(i).getTimeType()));
                LocalData.add(ctx,"line_DistrictOnLine", lines.get(i).getDistrictOnLine());
                LocalData.add(ctx,"line_CarType", lines.get(i).getCarType());
                LocalData.add(ctx,"line_CloseComment", String.valueOf(lines.get(i).getCloseComment()));
                LocalData.add(ctx,"line_CloseRequest", String.valueOf(lines.get(i).getCloseRequest()));
                LocalData.add(ctx,"line_Delete", String.valueOf(lines.get(i).getDelete()));
                LocalData.add(ctx,"line_Title", lines.get(i).getTitle());



                CU.openfrg(ctx, new frag_chatting(), false);
                LocalData.add(ctx, "CURRENT_LINE_ID", String.valueOf(lines.get(i).getId()));
            }

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CU.AddAnimation(view, motionEvent);
                return super.onTouch(view, motionEvent);
            }
        });


        return view;
    }
}

class my_lines_holder {
    TextView title;
    TextView capacity;
    TextView from_city;
    TextView destination;

    public my_lines_holder(View view) {
        title = (TextView) view.findViewById(R.id.row_line_header_title);
        capacity = (TextView) view.findViewById(R.id.row_line_header_capacity);
        from_city = (TextView) view.findViewById(R.id.row_line_header_from_city);
        destination = (TextView) view.findViewById(R.id.row_line_header_destination);

    }
}
