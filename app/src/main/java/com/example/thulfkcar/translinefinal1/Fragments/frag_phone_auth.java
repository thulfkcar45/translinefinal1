package com.example.thulfkcar.translinefinal1.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static android.content.ContentValues.TAG;
import static com.google.firebase.auth.FirebaseAuth.getInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_phone_auth extends Fragment {
    ProgressDialog progress1;

    //region prop
    View view;
    LinearLayout frag_phone_auth_verfiy_layout;
    LinearLayout frag_phone_auth_num_layout;
    EditText frag_phone_auth_text_vcode;
    LinearLayout frag_phone_auth_btn_verify;
    LinearLayout frag_phone_auth_btn;
    EditText frag_phone_auth_text_number;
    private OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private boolean mVerificationInProgress;
    private FirebaseAuth mAuth;
    //endregion

    public frag_phone_auth() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((DrawerLocker) getActivity()).setDrawerLocked(false,false);

        //region views
        view = inflater.inflate(R.layout.frag_phone_auth, container, false);
        frag_phone_auth_btn = (LinearLayout) view.findViewById(R.id.frag_phone_auth_btn);
        frag_phone_auth_text_number = (EditText) view.findViewById(R.id.frag_phone_auth_text_number);
        frag_phone_auth_verfiy_layout = (LinearLayout) view.findViewById(R.id.frag_phone_auth_verfiy_layout);
        frag_phone_auth_num_layout = (LinearLayout) view.findViewById(R.id.frag_phone_auth_num_layout);
        frag_phone_auth_text_vcode = (EditText) view.findViewById(R.id.frag_phone_auth_text_vcode);
        frag_phone_auth_btn_verify = (LinearLayout) view.findViewById(R.id.frag_phone_auth_btn_verify);
        //endregion
        mAuth = getInstance();
        // mAuth.setLanguageCode("iq");
        mAuth.useAppLanguage();

        return view;
//        FirebaseAuth.getInstance().signOut();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_phone_auth");

        MainActivity.set_top_nav(getActivity(), View.GONE);

        super.onViewCreated(view, savedInstanceState);
        frag_phone_auth_btn_verify.setEnabled(false);
        frag_phone_auth_btn_verify.setVisibility(View.INVISIBLE);
        mCallbacks = new OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                Toast.makeText(getContext(), "يرجى ادخال رقم هاتف صالح", Toast.LENGTH_SHORT).show();

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                LocalData.add(getActivity(), "verificationId", verificationId);
                Toast.makeText(getContext(), "الية التاكيد verificationId", Toast.LENGTH_LONG).show();
                frag_phone_auth_verfiy_layout.setVisibility(View.VISIBLE);
                frag_phone_auth_num_layout.setVisibility(View.GONE);
                mVerificationId = verificationId;
                mResendToken = token;

                // ...
            }
        };
//        startPhoneNumberVerification("07803947184");
        //  verifyPhoneNumberWithCode("AM5PThAdDlfL5w13-4EisdVoXKgLS0OxVdOo5-M34LnqC-R0VTDkQ8y5cwcbjzAqEZibRNS5r5qpqmB7L861m44phvyqcF94DFb8e5H6TKZDZduOJsn7QVI91jZDy7HsLkt_hLiwfeqeL0SML-jhXB0jEUEhYazCqA","351964");


        frag_phone_auth_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress1 = ProgressDialog.show(new ContextThemeWrapper(getActivity(), R.style.progressDialog), "تسجيل الدخول",
                        "يرجى الانتظار قليلاً ....", true);
                startPhoneNumberVerification(frag_phone_auth_text_number.getText().toString());
                CU.hideKeyboardFrom(getContext(),view);
                frag_phone_auth_text_vcode.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        if (s.toString().trim().length() == 0) {
                            frag_phone_auth_btn_verify.setEnabled(false);
                            frag_phone_auth_btn_verify.setVisibility(View.INVISIBLE);
                            //     frag_phone_auth_btn_verify.setBackground(Drawable.createFromPath("drawable/send_message_input_text.xml"));
                        } else {
//                            frag_phone_auth_btn_verify.setBackground(Drawable.createFromPath("drawable/drawable/roundedclicklong.xml"));

                            frag_phone_auth_btn_verify.setEnabled(true);
                            frag_phone_auth_btn_verify.setVisibility(View.VISIBLE);
                        }


                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // TODO Auto-generated method stub

                    }
                });
                frag_phone_auth_btn_verify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        verifyPhoneNumberWithCode(LocalData.get(getActivity(), "verificationId"), frag_phone_auth_text_vcode.getText().toString());
                        CU.hideKeyboardFrom(getContext(),view);
                    }
                });
            }
        });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            Toast.makeText(getContext(), user.getUid() + "", Toast.LENGTH_SHORT).show();
                            LocalData.add(getActivity(), "phone_auth_id", String.valueOf(user.getUid()));
                            BgLoginOrSingup bgLoginOrSingup = new BgLoginOrSingup(getActivity());
                            bgLoginOrSingup.execute();
                            // ...
                        } else {
                            Toast.makeText(getContext(), "خطاء في المصادقة حاول مرة اخرى", Toast.LENGTH_SHORT).show();
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(getContext(), "رمز التاكيد الذي ادخلته خاطئ يرجى التاكد", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    private class BgLoginOrSingup extends AsyncTask<String, Void, String> {
        Context context;
        User user;

        //ProgressBar pr;
        BgLoginOrSingup(Context ctx) {
            context = ctx;
        }

        @Override
        protected String doInBackground(String... params) {
            String login_url = PV.loginOrSignUpUrl();
            try {
                URL object = new URL(login_url);
                final HttpURLConnection con = (HttpURLConnection) object.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("Facebook", LocalData.get(context, "phone_auth_id"));
                parent.put("PhoneNum", LocalData.get(context, frag_phone_auth_text_number.getText().toString()));
                parent.put("Driver", LocalData.get(context, "FLAG_DRIVER_SATAE"));


                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                int HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }
                Log.v("thugj", result + "result");

                JSONObject main = new JSONObject(result);

                final int Id = main.getInt("Id");
                final String FacebookId = main.getString("FacebookId");
                final String Name = main.getString("Name");
                final String Photo = main.getString("Photo");
                final String PhoneNum = main.getString("PhoneNum");
                final String Address = main.getString("Address");
                final String GPSLocation = main.getString("GPSLocation");
                final String BirthDate = main.getString("BirthDate");
                final String Gender = main.getString("Gender");
                final String UserType = main.getString("UserType");

                user = new User() {{
                    setId(Id);
                    setName(Name);
                    setFacebookId(FacebookId);
                    setPhoto(Photo);
                    setPhoneNum(PhoneNum);
                    setAddress(Address);
                    setGpsLocation(GPSLocation);
                    setBirthDate(BirthDate);

                    if (Gender.contains("ذكر")) {
                        setGender(2);
                    } else {
                        setGender(1);
                    }

                    setUserType(Integer.parseInt(UserType));


                }};
                LocalData.add(context, "Id", String.valueOf(user.getId()));
                return result;


            } catch (MalformedURLException e) {
                Log.v("thug", e.toString() + "1");

                e.printStackTrace();
            } catch (IOException e) {
                Log.v("thug", e.toString() + "2");

                e.printStackTrace();
            } catch (JSONException e) {
                Log.v("thug", e.toString() + "3");

                e.printStackTrace();
            }
            return null;
        }

        protected void onPreExecute() {
            //   fragRegiserDriverBtnRegister.setText("تسجيل الدخول ...");

        }

        @Override
        protected void onPostExecute(String result) {

            progress1.dismiss();
            LocalData.add(getActivity(), "FaceBookId", user.getFacebookId());
            LocalData.add(getActivity(), "Id", String.valueOf(user.getId()));
            LocalData.add(getActivity(), "Name", user.getName());
            LocalData.add(getActivity(), "Address", user.getAddress());
            LocalData.add(getActivity(), "PhoneNum", user.getPhoneNum());
            LocalData.add(getActivity(), "Photo", user.getPhoto());
            LocalData.add(getActivity(), "Gender", String.valueOf(user.getGender()));
            LocalData.add(getActivity(), "GPSLocation", String.valueOf(user.getGpsLocation()));
            LocalData.add(getActivity(), "BirthDate", String.valueOf(user.getBirthDate()));
            LocalData.add(getActivity(), "Gender", String.valueOf(user.getUserType()));


            CU.openfrg(getActivity(), new frag_Main(), false);


        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }


    }

}
