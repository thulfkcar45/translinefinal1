package com.example.thulfkcar.translinefinal1.Fragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Classes.PV;
import com.example.thulfkcar.translinefinal1.DrawerLocker;
import com.example.thulfkcar.translinefinal1.MainActivity;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;
import com.example.thulfkcar.translinefinal1.adapters.my_lines_adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static java.lang.System.out;

/**
 * A simple {@link Fragment} subclass.
 */
public class frag_Main extends Fragment {

    GridView frag_main_My_lines;
    LinearLayout frag_main_add_line;
    ArrayList<Line> lines_headers = new ArrayList<>();
    Adapter my_lines_adapter;
    LinearLayout frag_main_search_Lines;
    SwipeRefreshLayout frag_mainSwipeRefresh;
    View view;

    public frag_Main() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.frag__main, container, false);
        ((DrawerLocker) getActivity()).setDrawerLocked(true,false);

        MainActivity.set_top_nav(getActivity(), view.GONE);
        //region views
        frag_main_My_lines = (GridView) view.findViewById(R.id.frag_main_My_lines);
        frag_main_add_line = (LinearLayout) view.findViewById(R.id.frag_main_add_line);
        frag_main_search_Lines = (LinearLayout) view.findViewById(R.id.frag_main_search_Lines);
        frag_mainSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.frag_mainSwipeRefresh);
        //endregion

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LocalData.add(getContext(), "CURENT_FRAGMENT", "frag_Main");

        //region toolBar
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.titel_nav_image);
        ImageView _2_image = (ImageView) toolbar.findViewById(R.id._2_nav_image);
        ImageView _1_image = (ImageView) toolbar.findViewById(R.id._1_nav_image);

        _1_image.setVisibility(View.VISIBLE);
        title.setVisibility(View.GONE);
        title.setText("");
        _2_image.setImageResource(R.drawable.tl_back_icon);
        _2_image.setClickable(false);
        _2_image.setVisibility(View.VISIBLE);
        // ((DrawerLocker) getActivity()).setDrawerLocked(false);


        CU.toolbarColor(getActivity(), "#FE5101");
        CU.toolbarTitle(getActivity(), null);

        if (LocalData.get(getActivity(), "FLAG_DRIVER_SATAE").equals("false")) {

            _2_image.setImageResource(R.drawable.tl_passenger_main_icon);

        } else {

            _2_image.setImageResource(R.drawable.tl_driver_main_icon);


        }
        _2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        //endregion

        CU.hideKeyboardFrom(getContext(), view);


        CU.nav_menue_items_set(getActivity());
//add line

        //get Line


        frag_main_search_Lines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CU.openfrg(getActivity(), new frag_search_line(), true);
            }
        });

        frag_main_add_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocalData.add(getContext(), "FLAG_EDIT_LINE", "false");

                CU.openfrg(getActivity(), new frag_add_line(), true);

            }
        });
        frag_mainSwipeRefresh.setRefreshing(true);
        bgGetMyLines bgGetMyLines = new bgGetMyLines(getContext());
        bgGetMyLines.execute(LocalData.get(getActivity(), "Id"));

        frag_mainSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                bgGetMyLines bgGetMyLines = new bgGetMyLines(getContext());
                bgGetMyLines.execute(LocalData.get(getActivity(), "Id"));
            }
        });

    }

    private class bgGetMyLines extends AsyncTask<String, Void, String> {
        Context context;

        public bgGetMyLines(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {

            String userId = params[0];


            String UpdateUser_URL = PV.getUserLinesUrl();
            try {
                URL object = new URL(UpdateUser_URL);

                HttpURLConnection con = (HttpURLConnection) object.openConnection();

                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");

                JSONObject parent = new JSONObject();

                parent.put("userId", userId);
                parent.put("Driver", LocalData.get(getActivity(), "FLAG_DRIVER_SATAE"));


                OutputStream wr = con.getOutputStream();
                wr.write(parent.toString().getBytes("UTF-8"));
                wr.flush();
                String result = "";
                int HttpResult = con.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                    br.close();
                }


                JSONArray jsonArray = new JSONArray(result);
                final ArrayList<User> users = new ArrayList<>();
                lines_headers.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject object1 = jsonArray.getJSONObject(i);

                    User user = new User() {{
                        setId(Integer.parseInt(LocalData.get(context, "Id")));

                    }};


                    users.add(user);
                    final Line line = new Line() {{
                        setId(object1.getInt("Id"));
                        setUsers(users);
                        setFromCity(object1.getString("FromCity"));
                        setDistenation(object1.getString("Distenation"));
                        setType(object1.getInt("Type"));
                        setPassCount(object1.getInt("PassengersCount"));
                        setPassCapacity(object1.getInt("PassengersCapacity"));
                        setPassType(object1.getInt("PassengerType"));
                        setGoTime(object1.getString("GoTime"));
                        setReturnTime(object1.getString("ReturnTime"));
                        setTimeType(object1.getInt("TimeType"));
                        setDistrictOnLine(object1.getString("DistrictOnLine"));
                        setCarType(object1.getString("CarType"));
                        setCloseComment(object1.getBoolean("CloseComment"));
                        setCloseRequest(object1.getBoolean("CloseRequest"));
                        setDelete(object1.getBoolean("Delete"));
                        setTitle(object1.getString("Title"));
                    }};
                    out.println(result);

                    lines_headers.add(line);


                }



                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            frag_mainSwipeRefresh.setRefreshing(false);


            my_lines_adapter = new my_lines_adapter(getActivity(), lines_headers);

            frag_main_My_lines.setAdapter((ListAdapter) my_lines_adapter);


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }


}
