package com.example.thulfkcar.translinefinal1.Classes;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.thulfkcar.translinefinal1.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by thulfkcar on 9/12/2017.
 */


public class SetTime implements TimePickerDialog.OnTimeSetListener, View.OnClickListener {
    Context ctx;
    private TextView textView;
    private Calendar myCalender;
    public SetTime(TextView textView,LinearLayout dialog, Context ctx) {
        this.textView = textView;
        dialog.setOnClickListener(this);
        this.myCalender = Calendar.getInstance();
        this.ctx = ctx;
    }

    @Override
    public void onTimeSet(TimePicker view, int houresOfDay, int minutes) {

        final String _24HourTime = houresOfDay + ":" + minutes;
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
//            System.out.println(_24HourDt);
//            System.out.println(_12HourSDF.format(_24HourDt));
            this.textView.setText(_12HourSDF.format(_24HourDt));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {


        int houres = myCalender.get(Calendar.HOUR_OF_DAY);
        int minutes = myCalender.get(Calendar.MINUTE);
        new TimePickerDialog(ctx, R.style.TimePicker, this, houres, minutes, false).show();
    }
}
