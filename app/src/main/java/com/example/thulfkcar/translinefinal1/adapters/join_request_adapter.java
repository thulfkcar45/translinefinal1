package com.example.thulfkcar.translinefinal1.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.thulfkcar.translinefinal1.Classes.CU;
import com.example.thulfkcar.translinefinal1.Classes.LocalData;
import com.example.thulfkcar.translinefinal1.Moudel.Line;
import com.example.thulfkcar.translinefinal1.Moudel.User;
import com.example.thulfkcar.translinefinal1.R;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by thulfkcar on 11/15/2017.
 */

public class join_request_adapter extends BaseAdapter {
    LayoutInflater inflater;

    Context ctx;
    ArrayList<User> users;
    private String phoneNum;

    public join_request_adapter(Context ctx, ArrayList<User> users) {
        this.ctx = ctx;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return users.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {

        if (inflater == null) {
            inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null) {
            view = inflater.inflate(R.layout.row_join_requet, viewGroup, false);
        }

        join_request_holder holder=new join_request_holder(view);
        if (users.get(i).getUserType()==1)
        holder.join_request_user_type.setText("سائق");
        else
            holder.join_request_user_type.setText("راكب");
                Glide
                    .with(ctx)
                .load(users.get(i).getPhoto())
                .placeholder(R.drawable.tl_avatar_icon)
                .error(R.drawable.tl_avatar_icon)
                .into(holder.join_request_userphoto
                );
        holder.join_request_user_name.setText(users.get(i).getName());
        Date date=new Date(users.get(i).getDate());
        try {
            holder.join_request_date.setText(CU.date_befor(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        phoneNum=users.get(i).getPhoneNum();
        holder.join_request_user_phone_num.setText(users.get(i).getPhoneNum());
        holder.join_request_user_address.setText(users.get(i).getAddress());
        holder.join_request_btnDail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(String.valueOf("tel:" + phoneNum)));
                ctx.startActivity(intent);
            }
        });
        holder.join_request_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Line.PostMake_request(ctx,
                        String.valueOf(users.get(i).getId()),
                        LocalData.get(ctx,"CURRENT_LINE_ID"),
                        "Delete");
                viewGroup.setVisibility(View.GONE);
            }
        });
        holder.join_request_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Line.PostMake_request(ctx,
                        String.valueOf(users.get(i).getId()),
                        LocalData.get(ctx,"CURRENT_LINE_ID"),
                        "Agree");
                viewGroup.setVisibility(View.GONE);
            }
        });

        return view;
    }
}

class join_request_holder {
    TextView join_request_user_type;
    ImageView join_request_userphoto;
    TextView join_request_date;
    TextView join_request_user_name;
    TextView join_request_user_phone_num;
    TextView join_request_user_address;
    LinearLayout join_request_btnDail;
    LinearLayout join_request_accept;
    LinearLayout join_request_reject;

    public join_request_holder(View view) {
         join_request_user_type=(TextView)view.findViewById(R.id.join_request_user_type);
        join_request_date=(TextView)view.findViewById(R.id.join_request_date);
         join_request_user_name=(TextView)view.findViewById(R.id.join_request_user_name);
         join_request_user_phone_num=(TextView)view.findViewById(R.id.join_request_user_phone_num);
         join_request_user_address =(TextView)view.findViewById(R.id.join_request_user_address);
         join_request_btnDail=(LinearLayout) view.findViewById(R.id.join_request_btnDail);
         join_request_accept=(LinearLayout)view.findViewById(R.id.join_request_accept);
         join_request_reject=(LinearLayout)view.findViewById(R.id.join_request_reject);
        join_request_userphoto=(ImageView) view.findViewById(R.id.join_request_userphoto);
    }
}